<?php
/**
 * WposVentivAPIs is part of Wallace Point of Sale system (WPOS) API
 *
 * WposSetup handles setup of pos devices, including first time setup and other device specific configuration.
 * It also provides device/user specific config records
 *
 * WallacePOS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * WallacePOS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details:
 * <https://www.gnu.org/licenses/lgpl.html>
 *
 * @package    wpos
 * @copyright  Copyright (c) 2014 WallaceIT. (https://wallaceit.com.au)
 * @link       https://wallacepos.com
 * @author     Michael B Wallace <micwallace@gmx.com>
 * @since      File available since 14/12/13 07:46 PM
 */
class WposPosVentivAPIs
{
    /**
     * @var mixed
     */
    private $data;

    /**
     * Decode provided JSON and extract commonly used variables
     * @param $data
     */
    public function __construct($data = null){
        $this->data = $data;
    }

   public function createLane(){
      $url = "https://triposcert.vantiv.com/cloudapi/v1/lanes/";
      $curl = curl_init($url);
      // error_log(var_export($curl));

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
         'Content-Type: application/json',
         'accept: application/json',
         'tp-application-id: 8557',
         'tp-application-name: bottlepos',
         'tp-application-version: 1.0.0',
         'tp-authorization: Version= 2.0',
         'tp-express-acceptor-id: 3928907',
         'tp-express-account-id: 1048395',
         'tp-express-account-token: F0091DBB09972882F30F20F2F3C98B98B32EAF8F7035D882880FCEE4804261DDFF4FEF01',
         'tp-request-id: 1546058f-5a25-4334-85ae-e68f2a44bbaf'
      ));

      $response = curl_exec($curl);
      $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
      $header = substr($response, 0, $header_size);
      $image_binary = substr($response, $header_size);
      curl_close($curl);
      return $response;
   }

    public function getLanes(){
      // $post_data = 'request = {}'
      //    "laneId":10,
      //    "description" : "CERTDEVICE",
      //    "terminalId":"60C798A524B5",
      //    "serialNumber":"262-039-494",
      //    "activationCode":"IIK6DE3",
      //    "modelNumber":"MX915"
      // );
      $url = "https://triposcert.vantiv.com/cloudapi/v1/lanes/";
      $curl = curl_init($url);
      // error_log(var_export($curl));

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
         'Content-Type: application/json',
         'accept: application/json',
         'tp-application-id: 8557',
         'tp-application-name: bottlepos',
         'tp-application-version: 1.0.0',
         'tp-authorization: Version= 2.0',
         'tp-express-acceptor-id: 3928907',
         'tp-express-account-id: 1048395',
         'tp-express-account-token: F0091DBB09972882F30F20F2F3C98B98B32EAF8F7035D882880FCEE4804261DDFF4FEF01',
         'tp-request-id: 1546058f-5a25-4334-85ae-e68f2a44bbaf'
      ));
      curl_setopt($curl, CURLOPT_POST, true);
      // curl_setopt($curl, CURLOPT_POSTFIELDS,'request':{});

      $response = curl_exec($curl);
      $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
      $header = substr($response, 0, $header_size);
      $image_binary = substr($response, $header_size);
      curl_close($curl);

        return $response;
    }

    public function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public function saleTransaction($result){
      $tp_request_id = $this->GUID();
      // $post_data = array("laneId" => $this->$data->laneId, "transactionAmount" => $this->data->transactionAmount);
      // $post_data = array("laneId" => 9999, "transactionAmount" => 10.22);

      $url = "https://triposcert.vantiv.com/api/v1/sale";
      $curl = curl_init($url);

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
         'Content-Type: application/json',
         'accept: application/json',
         'tp-application-id: 8557',
         'tp-application-name: bottlepos',
         'tp-application-version: 1.0.0',
         'tp-authorization: Version= 2.0',
         'tp-express-acceptor-id: 3928907',
         'tp-express-account-id: 1048395',
         'tp-express-account-token: F0091DBB09972882F30F20F2F3C98B98B32EAF8F7035D882880FCEE4804261DDFF4FEF01',
         'tp-request-id:'.$tp_request_id
      ));
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($_POST));

      $result = curl_exec($curl);
      $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
      $header = substr($result, 0, $header_size);
      $image_binary = substr($result, $header_size);
      curl_close($curl);

       return json_decode($result);
    }
}
