<?php
/**
 * ReportsModel is part of Wallace Point of Sale system (WPOS) API
 *
 * ReportsModel extends the DbConfig PDO class to interact with the config DB table
 *
 * WallacePOS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * WallacePOS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details:
 * <https://www.gnu.org/licenses/lgpl.html>
 *
 * @package    wpos
 * @copyright  Copyright (c) 2014 WallaceIT. (https://wallaceit.com.au)

 * @link       https://wallacepos.com
 * @author     Michael B Wallace <micwallace@gmx.com>
 * @since      File available since 11/23/13 10:36 PM
 */

class AccountingModel extends DbConfig
{


    /**
     * Init DB
     */
    public function __construct()
    {
        parent::__construct();
    }
	
	public function savecashreconciliation($data){
		//print_r($data);
		//die("DATA");
		//~ $sql = "INSERT INTO register_closing (`timeStamp`, `deviceid`, `userid`,`locationid`,`location`,`sales`,`refunds`,`voids`,`takings`,`totalcount`,`balance`,`userinput`) VALUES (:timeStamp, :deviceid, :userid, :locationid, :location, :sales, :refunds, :voids, :takings, :totalcount, :balance, :userinput);";
		$sql = "INSERT INTO register_closing (`timeStamp`, `deviceid`, `userid`,`locationid`,`location`,`sales`,`refunds`,`voids`,`takings`,`totalcount`,`balance`,`userinput`) VALUES ('".$data->timeStamp."', '".$data->deviceid."', '".$data->userid."', '".$data->locationid."', '".$data->location."', '".str_replace(',','',str_replace('$','', $data->sales))."', '".str_replace(',','',str_replace('$', '', $data->refunds))."', '".str_replace(',','',str_replace('$', '', $data->voids))."', '".str_replace(',','',str_replace('$', '', $data->takings))."', '".str_replace(',','',str_replace('$', '', $data->total))."', '".str_replace(',','',str_replace('$', '', $data->balance))."', '".json_encode($data->data)."');";
        //~ $placeholders = [":timeStamp"=>$data->timeStamp,":deviceid"=>$data->deviceid, ":userid"=>$data->userid,":locationid"=>$data->locationid,":location"=>$data->location,":sales"=>str_replace('$','', $data->sales),":refunds"=>str_replace('$', '', $data->refunds),":voids"=>str_replace('$', '', $data->voids),":takings"=>str_replace('$', '', $data->takings),":totalcount"=>str_replace('$', '', $data->total),":balance"=>str_replace('$', '', $data->balance),":userinput"=>json_encode($data->data)];
	
		$saved = $this->insert($sql, $placeholders);
		if ($saved===false || empty($saved))
					return false;
		
        return $saved;
		
	}
	 /**
     * @param null $Id
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
     public function getallclosing($Id = null) {

        $sql = 'SELECT * FROM register_closing';
        $placeholders = [];
        if ($Id !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' id = :id';
            $placeholders[':id'] = $Id;
        }
     
        $items = $this->select($sql, $placeholders);
        if ($items===false)
            return false;
        return $items;
    }
    public function deleteclosing($id){
		$placeholders = [];
        $sql = "DELETE FROM register_closing WHERE";
        if (is_numeric($id)){
            $sql .= " `id`=:id;";
            $placeholders[":id"] = $id;
        } else if (is_array($id)) {
            $id = array_map([$this->_db, 'quote'], $id);
            $sql .= " `id` IN (" . implode(', ', $id) . ");";
        } else {
            return false;
        }

        return $this->delete($sql, $placeholders);
		
	}
	public function closingedit($data){
		$usql = "UPDATE register_closing SET updated_at=now(),timeStamp = '".$data->timeStamp."' ,sales = '".$data->sales."' , refunds = '".$data->refunds."', voids = '".$data->voids."' , takings = '".$data->takings."'  , totalcount = '".$data->totalcount."' , balance = '".$data->balance."' WHERE id= '".$data->id."'";
		return $this->update($usql);
	}
	
}
