<?php
/**
 * OrderItemsModel is part of Wallace Point of Sale system (WPOS) API
 *
 * OrderItemsModel extends the DbConfig PDO class to interact with the config DB table
 *
 * WallacePOS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * WallacePOS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details:
 * <https://www.gnu.org/licenses/lgpl.html>
 *
 * @package    wpos
 * @copyright  Copyright (c) 2014 WallaceIT. (https://wallaceit.com.au)

 * @link       https://wallacepos.com
 * @author     Michael B Wallace <micwallace@gmx.com>
 * @since      File available since 11/23/13 10:36 PM
 */

class OrderItemsModel extends DbConfig
{


    /**
     * Init DB
     */
    public function __construct()
    {
        parent::__construct();
    }
	
	/**
    * @param $data(supplier id)
    * @return array|bool Returns false on an unexpected failure or an array of items having quantity less than the restore point having same supplier
    */
    public function getItemsWithLowQuantity($data){
		
		$sql = "SELECT si. * , sl.stocklevel AS currentquantity, sl.reorder_value, sl.reorder_point
				FROM stored_items si
				INNER JOIN stock_levels sl ON ( si.id = sl.storeditemid
				AND sl.stocklevel <= sl.reorder_point )"; 
				
		if($data!='all'){
			$sql .= ' WHERE si.supplierid = '.$data.'';
		}

		$items = $this->select($sql, $placeholders);	
		if ($items===false || empty($items))
            return false;
		
		
        return $items;	
	}
	/**
    * @param $data(type item name)
    * @return array|bool Returns false on an unexpected failure or an array of items having quantity less than the restore point having same supplier
    */
	public function sugestingItemsWithLowQuantity($data){
		$code = $data->find;
		$sql = "SELECT si. * , sl.stocklevel AS currentquantity, sl.reorder_value , sl.reorder_point 
				FROM stored_items si
				INNER JOIN stock_levels sl ON ( si.id = sl.storeditemid)"; 
				
		if($data->enter == 0){
				$sql .= " WHERE name 
					LIKE '%".$code."%'
					OR code 
					LIKE '%".$code."%'";
					
		}
		else if($data->enter == 1){
				$sql .= " WHERE code = '".$code."' OR name = '".$code."'";

		}
		$items = $this->select($sql, $placeholders);	
		if ($items===false || empty($items))
            return false;
		
	
        return $items;	
		
	}
    /**
     * @param $data
     * @return bool|string Returns false on an unexpected failure, returns -1 if a unique constraint in the database fails, or the new rows id if the insert is successful
     */
     
	public function addOrders($data){

		$sql = "INSERT INTO orders (`order_date`, `order_number`, `order_total`,`order_supplier_id`) VALUES (now(), :order_number, :order_total, :order_supplier_id);";
        $placeholders = [":order_number"=>$data['order_number'],":order_total"=>$data['totals_sum'], ":order_supplier_id"=>$data['supplierid']];
		
		$order = $this->insert($sql, $placeholders);
		
        return $order;

	}
	public function updateOrders($data){
		$usql = "UPDATE orders SET order_total = '".$data['order_total']."' ,order_supplier_id = '".$data['order_supplier_id']."' , order_date = now() WHERE id= '".$data['id']."'";
		return $this->update($usql);
	}
	
	public function deleteOrder($data){
		$sql = "DELETE o.*, oi.*
				FROM orders o
				LEFT JOIN order_items oi ON o.id = oi.order_id
				WHERE o.id = '".$data->orderid."'";
		$deleteid = $this->delete($sql);
		return $deleteid;
	}
	public function deleteItemsofspecificorder($data){

		$sql = "DELETE  oi.*
				FROM orders o
				LEFT JOIN order_items oi ON o.id = oi.order_id
				WHERE o.id = '".$data['id']."'";
		$deleteid = $this->delete($sql);
		return $deleteid;
	
	}
	public function editOrderitems($data){

		$sql = "UPDATE order_items SET item_name= :item_name, item_cost= :item_cost, item_price= :item_price, item_reorder_quantity= :item_reorder_quantity WHERE id= :id;";
        $placeholders = [":id"=>$data->id, ":item_name"=>$data->itemname, ":item_cost"=>$data->itemcost, ":item_price"=>$data->itemprice, ":item_reorder_quantity"=>$data->itemreorderqty];
        $update = $this->update($sql, $placeholders);
        
        $isql = "SELECT oi . *
				FROM order_items oi
				LEFT JOIN orders o ON o.id = oi.order_id
				WHERE o.id ='".$data->orderid."'";
		$items = $this->select($isql, $placeholders);	
		
		$total = 0 ;
		foreach($items as $key => $value){
			$total = $total + $value['item_cost'] * $value['item_reorder_quantity'];
		}
		
		$newtotal = number_format((float)$total, 2, '.', '');
		$usql = "UPDATE orders SET order_total = '".$newtotal."' WHERE id= '".$data->orderid."'";
		$this->update($usql);	
        return  $update;
	}
	
	public function deleteOrderItem($data){
		
		$sql = "DELETE oi.*
				FROM order_items oi
				WHERE oi.id = '".$data->id."'";
		$deleteid = $this->delete($sql);
		
		$isql = "SELECT oi . *
				FROM order_items oi
				LEFT JOIN orders o ON o.id = oi.order_id
				WHERE o.id ='".$data->orderid."'";
		$items = $this->select($isql, $placeholders);	
		
		$total = 0 ;
		foreach($items as $key => $value){
			$total = $total + $value['item_cost'] * $value['item_reorder_quantity'];
		}
		
		$newtotal = number_format((float)$total, 2, '.', '');
		$usql = "UPDATE orders SET order_total = '".$newtotal."' WHERE id= '".$data->orderid."'";
		$this->update($usql);	
		return $deleteid;
		
	}
	
	 /**
     * @param null $Id
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
     public function getOrders($Id = null) {

        $sql = 'SELECT * FROM orders';
        $placeholders = [];
        if ($Id !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' id = :id';
            $placeholders[':id'] = $Id;
        }
     
        $items = $this->select($sql, $placeholders);
        if ($items===false)
            return false;
        return $items;
    }
 	 /**
     * @param $data
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */    
    public function addCurrentOrderItems($data){

		$sql = "INSERT INTO order_items (`order_id`, `item_code`, `item_cost`, `item_price`, `item_reorder_quantity`, `item_name`) VALUES (:order_id, :item_code, :item_cost, :item_price, :item_reorder_quantity, :item_name);";
        $placeholders = [":order_id"=>$data['order_id'], ":item_code"=>$data['code'], ":item_cost"=>$data['cost'], ":item_price"=>$data['price'], ":item_reorder_quantity"=>$data['reorderqty'], ":item_name"=>$data['name']];
		
		$orderitems = $this->insert($sql, $placeholders);
		
        return $orderitems;		
	}
 	 /**
     * @param $data(orderid)
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */	
	public function allItemsofOrder($data){
		$sql = "SELECT oi . *
				FROM order_items oi
				LEFT JOIN orders o ON o.id = oi.order_id
				WHERE o.id ='".$data->orderid."'";
			$items = $this->select($sql, $placeholders);	
		if ($items===false || empty($items))
            return false;
  
        return $items;	
	}
	public function allitemsandorder($data){
		/*$sql = "SELECT oi . *,o.order_date,o.order_total,o.order_supplier_id
				FROM order_items oi
				LEFT JOIN orders o ON o.id = oi.order_id
				WHERE o.id ='".$data->orderid."'"; */ // old query
		
		  $sql ="SELECT oi. * , o.order_date, o.order_total, o.order_supplier_id, si.data, sl.stocklevel AS currentquantity
				FROM order_items oi
				LEFT JOIN orders o ON o.id = oi.order_id
				LEFT JOIN stored_items si ON oi.item_code = si.code
				LEFT JOIN stock_levels sl ON sl.storeditemid = si.id
				WHERE o.id ='".$data->orderid."'";
			$items = $this->select($sql, $placeholders);	
		if ($items===false || empty($items))
            return false;
  
        return $items;	
	}
}
