<?php
/**
 * ReceiveItemsModel is part of Wallace Point of Sale system (WPOS) API
 *
 * ReceiveItemsModel extends the DbConfig PDO class to interact with the config DB table
 *
 * WallacePOS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * WallacePOS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details:
 * <https://www.gnu.org/licenses/lgpl.html>
 *
 * @package    wpos
 * @copyright  Copyright (c) 2014 WallaceIT. (https://wallaceit.com.au)

 * @link       https://wallacepos.com
 * @author     Michael B Wallace <micwallace@gmx.com>
 * @since      File available since 11/23/13 10:36 PM
 */

class ReceiveItemsModel extends DbConfig
{

    /**
     * @var array available columns
     */
    protected $_columns = ['id' ,'data', 'supplierid', 'code', 'name', 'price'];

    /**
     * Init DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $data
     * @return bool|string Returns false on an unexpected failure, returns -1 if a unique constraint in the database fails, or the new rows id if the insert is successful
     */
    public function create($data)
    {
		$sql = "INSERT INTO receive_items (`data`, `supplierid`, `name`,`code`, `price`, `cost`, `category_id`, `invoice_number`, `invoice_total`, `totals_sum`,`created`) VALUES (:data, :supplierid, :name, :code, :price, :cost, :categoryid, :invoice_number, :invoice_total, :totals_sum, :created);";
        $placeholders = [":data"=>json_encode($data),":supplierid"=>$data['supplierid'], ":name"=>$data['name'],":code"=>$data['code'], ":price"=>$data['price'], ":cost"=>$data['cost'], ":categoryid"=>$data['categoryid'], ":invoice_number"=>$data['invoice_number'], ":invoice_total"=>$data['invoice_total'], ":totals_sum"=>$data['totals_sum'], ":created"=>$data['created'] ];
    
        return  $this->insert($sql, $placeholders);
    }
    

    /**
     * @param null $Id
     * @param null $code
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
    public function get($num = null) {
		
		  
		$sql = 'SELECT * FROM receive_items';
        $placeholders = [];
        if ($Id !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' id = :id';
            $placeholders[':id'] = $Id;
        }
        if ($num !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' invoice_number = :invoice_number';
            $placeholders[':invoice_number'] = $num;
        }
       
        Logger::write("SQL query === ".json_encode($placeholders));
      
        $items = $this->select($sql, $placeholders);
       
        if ($items===false)
            return false;
            
        if ( empty($items))
            return $items;


        foreach($items as $key=>$item){
            $data = json_decode($item['data'], true);
            $data['id'] = $item['id'];
            $items[$key] = $data;
        }
	
        return $items;
    }
    
     public function getGroupedInvoiceNum($Id = null, $code = null) {
		/*$sql = "SELECT COUNT( * ) AS count, SUM( price ) AS price, SUM( cost ) AS cost, invoice_number, MAX( invoice_total ) AS invoice_total, MAX( supplierid ) AS supplierid, MAX( totals_sum ) AS totals_sum, MIN( created ) AS created
				FROM receive_items
				GROUP BY invoice_number
				ORDER BY `receive_items`.`id` DESC ";*/
		/*
		$sql= 'SELECT COUNT(*) as count ,SUM(price) as price,SUM(cost) as cost, invoice_number,MAX(invoice_total) AS invoice_total,MAX(supplierid) AS supplierid,
               MAX(totals_sum) AS totals_sum,MIN(created) AS created FROM receive_items 
               GROUP BY invoice_number';*/
		
		$sql = 'SELECT * FROM receive_items';

        $placeholders = [];
        if ($Id !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' id = :id';
            $placeholders[':id'] = $Id;
        }
        if ($code !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' code = :code';
            $placeholders[':code'] = $code;
        }
      
        $items = $this->select($sql, $placeholders);
           
        if ($items===false)
            return false;
            
        if (empty($items))
            return $items;

		$newarray=[];
       foreach($items as $key=>$item){
			//if($item['invoice_number'])
			 //$valuexist 		= $this->findKey($newarray,$item['invoice_number']);
			 //echo $valuexist . "</br>";
			//~ if($valuexist){
			//~ echo  " HHEE ";	
			//~ }
			//~ if(in_array($item['invoice_number'],$newarray)){
				//~ $newarray[$item['invoice_number']]['dddcost'] = $newarray[$item['invoice_number']]['dddcost'] + $data['cost']*$data['qty'];
				//~ $newarray[$item['invoice_number']]['dddprice'] = $newarray[$item['invoice_number']]['dddprice'] + $data['price']*$data['qty'];
				//~ $newarray[$item['invoice_number']]['count']= $newarray[$item['invoice_number']]['count'] + 1;
			//~ }
			
            $data = json_decode($item['data'], true);
          
            $data['id'] = $item['id'];
            //$newarray[$item['invoice_number']] = $data;
            $newarray[$item['invoice_number']]['cost']=$newarray[$item['invoice_number']]['cost'] + ($data['cost']*$data['qty']);
            $newarray[$item['invoice_number']]['price']=$newarray[$item['invoice_number']]['price']+ ($data['price']*$data['qty']);
            $newarray[$item['invoice_number']]['count']=++$newarray[$item['invoice_number']]['count'];
            $newarray[$item['invoice_number']]['invoice_number']=$item['invoice_number'];
            $newarray[$item['invoice_number']]['supplierid']=$item['supplierid'];
            $newarray[$item['invoice_number']]['invoice_total']=$item['invoice_total'];
            $newarray[$item['invoice_number']]['totals_sum']=$item['totals_sum'];
            $newarray[$item['invoice_number']]['created']=$item['created'];
            $newarray[$item['invoice_number']]['margin']=$newarray[$item['invoice_number']]['margin']+$data['margin'];
            //print_r($newarray);
            //die;
        }
 
         return $newarray;
    }
	public function findKey($array, $keySearch)
	{
		foreach ($array as $key => $item) {
			if ($key == $keySearch) {
				echo 'yes, it exists';
				return true;
			}
			else {
				//~ if (is_array($item) && $this->findKey($item, $keySearch)) {
				   //~ return true;
				//~ }
			}
		}

		return false;
	}
    /**
     * @param $id
     * @param $data
     * @return bool|int Returns false on an unexpected failure or the number of rows affected by the update operation
     */
    public function edit($id, $data){
        $sql = "UPDATE receive_items SET data= :data, supplierid= :supplierid, code= :code, name= :name, price= :price, cost = :cost, category_id = :categoryid, invoice_total = :invoice_total  WHERE id= :id;";
        $placeholders = [":id"=>$id, ":data"=>json_encode($data), ":supplierid"=>$data->supplierid, ":code"=>$data->code, ":name"=>$data->name, ":price"=>$data->price, ":cost"=>$data->cost, ":categoryid"=>$data->categoryid, ":invoice_total"=>$data->invoice_total];
        return $this->update($sql, $placeholders);
    }

    /**
     * @param integer|array $id
     * @return bool|int Returns false on an unexpected failure or the number of rows affected by the delete operation
     */
    public function remove($id){

        $placeholders = [];
        $sql = "DELETE FROM receive_items WHERE";
        if (is_numeric($id)){
            $sql .= " `id`=:id;";
            $placeholders[":id"] = $id;
        } else if (is_array($id)) {
            $id = array_map([$this->_db, 'quote'], $id);
            $sql .= " `id` IN (" . implode(', ', $id) . ");";
        } else {
            return false;
        }

        return $this->delete($sql, $placeholders);
    }
    
    /**
     * @param null $Id
     * @param null $code
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
    public function getReceiveItems($Id = null, $code = null, $supplierid = null) {
		 $placeholders = [];
		if($supplierid == 0){	
			$sql = 'SELECT receive_items.*,stored_suppliers.name as supplier FROM receive_items,stored_suppliers  ';
			if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' receive_items.supplierid=stored_suppliers.id';
            $placeholders[':supplierid'] = $supplierid;
          
		} else{
			$sql = 'SELECT receive_items.*,stored_suppliers.name as supplier FROM receive_items,stored_suppliers ';
	    }
       
        if ($Id !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' id = :id';
            $placeholders[':id'] = $Id;
        }
        if ($code !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' code = :code';
            $placeholders[':code'] = $code;
        }
        if ($supplierid != 0) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' receive_items.supplierid=stored_suppliers.id AND receive_items.supplierid= :supplierid';
            $placeholders[':supplierid'] = $supplierid;
        }
      
        $items = $this->select($sql, $placeholders);
        
        if ($items===false)
            return false;

        foreach($items as $key=>$item){
            $data = json_decode($item['data'], true);
            $data['supplier'] = $item['supplier'];
            $data['id'] = $item['id'];
            $items[$key] = $data;
        }
        return $items;
    }
    
    
     /**
     * @param null $Id
     * @param null $code
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
     public function getStoredItems($Id = null, $code = null) {
        $sql = 'SELECT * FROM stored_items';
        $placeholders = [];
        if ($Id !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' id = :id';
            $placeholders[':id'] = $Id;
        }
        if ($code !== null) {
            if (empty($placeholders)) {
                $sql .= ' WHERE';
            }
            $sql .= ' code = :code';
            $placeholders[':code'] = $code;
        }

        $items = $this->select($sql, $placeholders);
        if ($items===false)
            return false;

        foreach($items as $key=>$item){
            $data = json_decode($item['data'], true);
            $data['id'] = $item['id'];
            $items[$key] = $data;
        }

        return $items;
    }
    /**
     * @param $data
     * @return bool|string Returns false on an unexpected failure, returns -1 if a unique constraint in the database fails, or the new rows id if the insert is successful
     */
    public function createStoredItems($data)
    {
				
        $sql          = "INSERT INTO stored_items (`data`, `supplierid`, `categoryid`, `code`, `name`, `price`) VALUES (:data, :supplierid, :categoryid, :code, :name, :price);";
        $placeholders = [":data"=>json_encode($data),":supplierid"=>$data['supplierid'], ":categoryid"=>$data['categoryid'], ":code"=>$data['code'], ":name"=>$data['name'], ":price"=>$data['price']];
    
        return $this->insert($sql, $placeholders);
    }
     /**
     * @param null $Id
     * @param null $code
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
    public function getSearchedItems( $data) {
	
		$code = $data->find;
		if($data->enter == 0){
			
			$sql = "SELECT *
					FROM stored_items
					WHERE name LIKE '%".$code."%'
					OR code LIKE '%".$code."%'"; 
				
		}
		else if($data->enter == 1){
			$placeholders = [];
			if ($code !== null) {
				$sql = 'SELECT * FROM stored_items';
				if (empty($placeholders)) {
					$sql .= " WHERE";
				}
				$sql .= " code = '".$code."' OR name = '".$code."'";
			}
		}
		
		
        $items = $this->select($sql, $placeholders);
        
        if ($items===false || empty($items))
            return false;

       foreach($items as $key=>$item){
			$data = json_decode($item['data'], true);
            $data['id'] = $item['id'];
            $items[$key] = $data;
        }
       
        return $items;
    }
    
     
     /**
     * @param null $Id
     * @param null $code
     * @return array|bool Returns false on an unexpected failure or an array of selected rows
     */
    public function getSearchedItemsInGenericItemsTable( $data) {
		$code = $data->find;
		if($data->enter == 0){
			
			$sql = "SELECT *
					FROM generic_items
					WHERE code LIKE '%".$code."%'
					OR name LIKE '%".$code."%'"; 
				
		}
		else if($data->enter == 1){
			$placeholders = [];
			if ($code !== null) {
				$sql = 'SELECT * FROM generic_items';
				if (empty($placeholders)) {
					$sql .= " WHERE";
				}
				$sql .= " code = '".$code."' OR name = '".$code."'";
			}
		}

        $items = $this->select($sql, $placeholders);
     
        if ($items===false || empty($items))
            return false;
  

        return $items;
    }

     /**
     * @param $id
     * @param $data
     * @return bool|int Returns false on an unexpected failure or the number of rows affected by the update operation
     */
    public function updateOldStoredItem($code, $view){
		$sql = "UPDATE stored_items SET data= :data, supplierid= :supplierid, categoryid= :categoryid, code= :code, name= :name, price= :price WHERE code= :code;";
        $placeholders = [":data"=>json_encode($view), ":supplierid"=>$view['supplierid'], ":categoryid"=>$view['categoryid'], ":code"=>$view['code'], ":name"=>$view['name'], ":price"=>$view['price']];
		
		return $this->update($sql, $placeholders);
    }
    
     /**
     * @param $id
     * @param $data
     * @return bool|int Returns false on an unexpected failure or the number of rows affected by the update operation
     */
    public function updateOldReceiveItem($code, $viewStored){
		$sql = "UPDATE receive_items SET data= :data, supplierid= :supplierid, code= :code, name= :name, price= :price WHERE code= :code;";
        $placeholders = [":data"=>json_encode($viewStored), ":supplierid"=>$viewStored['supplierid'], ":code"=>$viewStored['code'], ":name"=>$viewStored['name'], ":price"=>$viewStored['price']];
       
        return $this->update($sql, $placeholders);
    }

	public function setStockLevel($storeditemid, $stocklevel,$stocknumber){
			
			$sql = 'SELECT * FROM stock_levels';
			$placeholders = [];
		   
			if ($storeditemid !== null) {
				if (empty($placeholders)) {
					$sql .= ' WHERE';
				}
				$sql .= ' storeditemid = :storeditemid';
				$placeholders[':storeditemid'] = $storeditemid;
			}
			
			$items = $this->select($sql, $placeholders);
			
	  if($items){	
			
			$type = "Stock Updated";
			$sql = "UPDATE stock_levels SET `stocklevel`= `stocklevel` + :stocklevel WHERE `storeditemid`=:storeditemid AND `locationid`=:locationid";
			$placeholders = [":storeditemid"=>$storeditemid, ":stocklevel"=>$stocklevel, ":locationid"=>1];
			
			
			$result=$this->update($sql, $placeholders);


			if ($result===false) // if error occured return
				return false;

		}
		else{
		
			$type = "Stock Added";
			$sql = "INSERT INTO stock_levels (`storeditemid`, `locationid`, `stocklevel`, `dt`, `reorder_value`, `reorder_point`,`stocknumber`) VALUES (:storeditemid, :locationid, :stocklevel, now(), :reorder_value, :reorder_point, :stocknumber);";
			$placeholders = [":storeditemid"=>$storeditemid, ":locationid"=>1, ":stocklevel"=>$stocklevel, ":reorder_value"=>0, ":reorder_point"=>0, ":stocknumber"=>$stocknumber];
			
			$result=$this->insert($sql, $placeholders);
		
		}
		 $sqlforstockhist = "INSERT INTO stock_history (storeditemid, locationid, auxid, auxdir, type, amount, dt) VALUES (:storeditemid, :locationid, :auxid, :auxdir, :type, :amount, '".date("Y-m-d H:i:s")."');";
		 $stockhistplaceholders = [":storeditemid"=>$storeditemid, ":locationid"=>1, ":auxid"=>-1, ":auxdir"=>0, ":type"=>$type, ":amount"=>$stocklevel];
		
		 $stockhistresult= $this->insert($sqlforstockhist, $stockhistplaceholders);
		 if ($result>0) // if row has been updated or inserted return
			return $result;
	}

}
