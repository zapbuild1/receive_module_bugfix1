<!-- WallacePOS: Copyright (c) 2014 WallaceIT <micwallace@gmx.com> <https://www.gnu.org/licenses/lgpl.html> -->
	<div class="page-header">
	    <h1 style="margin-right: 20px; display: inline-block;">
	        Order
	    </h1>
	      <button onclick="$('#addorderdialog').dialog('open');" id="addbtn" class="btn btn-primary btn-sm pull-right"><i class="icon-pencil align-top bigger-125"></i>Create Order</button>

	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12">	
					<div class="table-header">
						Manage your Orders
					</div>
					<table id="orderitemstable" class="table table-striped table-bordered table-hover dt-responsive" style="width:100%;">
						<thead>
							<tr>
								<th data-priority="3">ID</th>
								<th data-priority="3">Order Number</th>
								<th data-priority="2">Order Total</th>
								<th data-priority="2">Supplier</th>
								<th data-priority="5">Order Date</th>
								<th class="noexport" data-priority="2">Items</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
	
	<div id="addorderdialog" class="hide">	
	
		<div class="row form-group">
<!--
			<div class="col-sm-6"><label>Order Number:</label><input id="newordernumber" type="text" class="input-width" readonly placeholder="auto generated" required="true" /><br/></div>
-->
			<div class="col-sm-6"><label>Supplier:</label><select id="neworderitemsupplier" class="supselect" style="width: 70%;" required="true"></select></div>
			<div class="col-sm-6"><label>Total:</label><input id="neworderitemstotal" type="text" class="input-width" required="true" readonly="true" /><br/></div>
		</div>
		
		<div class="row form-group">
			<div class="col-sm-6"><label>Product:</label><div class="suggesstion-outer"><input id="neworderitemproduct" type="text" placeholder="Type product stock code and press enter"/><div id="suggestion-order-box"><ul></ul></div></div></div>
			<div class="col-sm-6"><label>Date:</label><input id="neworderitemdate" type="text" class="input-width" required="true" /><br/></div>
		</div>
		<hr>
		<div class="from-group">
			<div class="forscroll">
			<table class="table table-stripped table-responsive " id="neworderitemtabledetail" style="margin-bottom: 0; padding-left: 10px; margin-right: 10px;">
				<thead class="table-header smaller">
					<tr>
						<th><small>Stock Code</small></th>
						<th><small>Name</small></th>
						<th><small>Category</small></th>
						<th><small>Current Inventory</small></th>
						<th><small>Reorder Qty</small></th>
						<th><small>Cost</small></th>
						<th><small>Price</small></th>
						<th><small>Margin</small></th>
						<th><small>Total</small></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="neworderitemmodtable">
	
				</tbody>
			</table>
			</div>
		</div>
	</div>
	<div id="orderdatadialog" class="hide">
		<div style="width: 100%; overflow-x: auto;">
			<table id="detailOrderData" class="table table-responsive table-stripped">
				<thead>
					<tr>
						<th data-priority="1">ID</th>
						<th data-priority="3">Order Id</th>
						<th data-priority="2">Item Code</th>
						<th data-priority="2">Item Name</th>
					    <th data-priority="7">Item Cost</th>
						<th data-priority="4">Item Price</th>
						<th data-priority="5">Item Reorder Quantity</th>
						<th data-priority="2">Action</th>
						
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
	<div id="editorderitemsdialog" class="hide">
	    <div class="tabbable" style="min-width: 360px; min-height: 310px;">
	        <ul class="nav nav-tabs">
	            <li class="active">
	                <a href="#receiveitemdetails" data-toggle="tab">
	                    Details
	                </a>
	            </li>
	        </ul>
	        <div class="tab-content" style="min-height: 320px;">
	            <div class="tab-pane active in" id="receiveitemdetails">              
	                <table>
						<tr>
							<td style="text-align: right;"><label>Id</label></td>
							<td><input id="ordersitemsid" class="supselect" readonly style="width: 100% !important;">
								</select>
								<input id="ordersitemsid" type="hidden"/></td>
						</tr>
			
						<tr>
							<td style="text-align: right;"><label>Order Id:&nbsp;</label></td>
							<td><input id="ordersitemsorderid" type="text" readonly="true"/><br/></td>
							<input id="ordersitemordernumber" type="hidden"/>
							<input id="ordersitemordertotal" type="hidden"/>
							<input id="ordersitemsordersupplier" type="hidden"/>
							<input id="ordersitemsorderdate" type="hidden"/>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Item Code:&nbsp;</label></td>
							<td><input id="ordersitemsitemcode" type="text" value="0" readonly="true"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Item Name:&nbsp;</label></td>
							<td><input id="ordersitemsitemname" type="text" value="0"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Cost:&nbsp;</label></td>
							<td><input id="ordersitemsitemcost" type="text" value="1"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Price:&nbsp;</label></td>
							<td><input id="ordersitemsitemprice" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Reorder Quantity:&nbsp;</label></td>
							<td><input id="ordersitemsreorderqty" type="number"/></td>
						</tr>
						                
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="addItemsInCurrentOrder" class="hide">	
	<div class="row form-group">
<!--
			<div class="col-sm-6"><label>Order Number:</label><input id="newordernumber" type="text" class="input-width" readonly placeholder="auto generated" required="true" /><br/></div>
-->
			<div class="col-sm-6"><label class="dialog-class">Supplier:</label><select id="newaddorderitemsupplier" class="supselect" style="width: 70%;" required="true"></select></div>
			<div class="col-sm-6"><label class="dialog-class">Total:</label><input id="newaddorderitemstotal" type="text" class="input-width" required="true" readonly="true" /><br/></div>
		</div>
		
		<div class="row form-group">
			<div class="col-sm-6"><label class="dialog-class">Product:</label><div class="suggesstion-outer"><input class="product-field" id="newaddorderitemproduct" type="text" placeholder="Type product stock code and press enter"/><div id="suggestion-order-box-items"><ul></ul></div></div></div>
			<div class="col-sm-6"><label class="dialog-class">Date:</label><input id="newaddorderitemdate" readonly type="text" class="input-width" required="true" /><br/></div>
		</div>
		
		<hr>
		<div class="from-group">
			<div class="forscroll">
			<table class="table table-stripped table-responsive " id="addItemsInCurrentOrdertabledetail" style="margin-bottom: 0; padding-left: 10px; margin-right: 10px;">
				<thead class="table-header smaller">
					<tr>
						<th><small>Stock Code</small></th>
						<th><small>Name</small></th>
						<th><small>Category</small></th>
						<th><small>Current Inventory</small></th>
						<th><small>Reorder Qty</small></th>
						<th><small>Cost</small></th>
						<th><small>Price</small></th>
						<th><small>Margin</small></th>
						<th><small>Total</small></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="addItemsInCurrentOrdermodtable">
	
				</tbody>
			</table>
			</div>
		</div>
	</div>
	
	<script>
		var receivestock = null;
	    var stockhist = null;
	    var suppliers = null;
	    var categories = null;
		var datatable,datatables;
		$(document).ready(function(){
		
			var data1 = WPOS.sendJsonData("orders/getGroupedData", JSON.stringify({"orders/getGroupedData":""}));
	        var data2 = WPOS.sendJsonData("suppliers/get", JSON.stringify({"suppliers/get":""}));
	        var data3 = WPOS.sendJsonData("categories/get", JSON.stringify({"categories/get":""}));		
			
			receivestock = data1;
	        suppliers = data2;
	        categories = data3;
	        var receiveitemarray = [];
	        var tempitemreceive;
	        var taxrules = WPOS.getTaxTable().rules;
	        for (var key in receivestock){
	            tempitemreceive = receivestock[key];
	            if (taxrules.hasOwnProperty(tempitemreceive.taxid)){
	                tempitemreceive.taxname = taxrules[tempitemreceive.taxid].name;
	            } else {
	                tempitemreceive.taxname = "Not Defined";
	            }
	            receiveitemarray.push(tempitemreceive);
	        }
	        
	        
	        datatable = $('#orderitemstable').dataTable({
	            "bProcessing": true,
	            "aaData": receiveitemarray,
	            "aoColumns": [
	                { "mData":"id" },
	                { "mData":"order_number" },
	                { "mData":"order_total" },
	                { "mData":function(data,type,val){return (suppliers.hasOwnProperty(data.order_supplier_id)?suppliers[data.order_supplier_id].name:'None'); } },
	                { "mData":"order_date" },
	                { mData:function(data,type,val){ return '<div class="action-buttons"><a class="red" onclick=getOrderItemsOfNumber("'+data.id+'","'+data.order_number+'","'+data.order_total+'",$(this).closest(\'tr\').find(\'td\').eq(3).text(),$(this).closest(\'tr\').find(\'td\').eq(4).text());><i class="icon-time bigger-130"></i></a><a class="red" onclick=removeOrder("'+data.id+'");><i class="icon-trash bigger-130"></i></a><a class="" onclick=editCurrentOrder("'+data.id+'");><i class="icon-pencil bigger-130"></i></a></div>'; }, "bSortable": false },
	            ],   
	            "columns": [
	                {type: "numeric"},
	                {type: "numeric"},
	                {type: "string"},
	                {type: "string"},
	                {type: "string"},
	                {}
	            ]
	        });
	      
			//onclick=$("#addItemsInCurrentOrder").data("param_1",'+data.id+').dialog("open")
	        // populate category & supplier records in select boxes
						var supsel = $(".supselect");
						supsel.html('');
						supsel.append('<option class="supid-0" value="0">None</option>');
						for (key in suppliers){
							supsel.append('<option  class="supid-'+suppliers[key].id+'" value="'+suppliers[key].id+'">'+suppliers[key].name+'</option>');
						}
						supsel.append('<option class="supid-0" value="all">All</option>');
			 var now = new Date();
			  var month = now.getMonth()+1;
			  if(month<10) {
					month='0'+month
			  }
			  var day = now.getDate();
			  if(day<10) {
					day='0'+day
			  }
			  var date = month+'/'+day+'/'+now.getFullYear(); 
			  $("#neworderitemdate").val(date);
			 
			  $("#neworderitemdate").datepicker();
			  
			  // hide loader
			  WPOS.util.hideLoader();
		});
		// dialogs
		 $( "#addorderdialog" ).removeClass('hide').dialog({
				resizable: false,
				width: 'auto',
				modal: true,
				autoOpen: false,
				position: { my: 'top', at: 'top+100' },
				title: "Order Items",
				title_html: true,
				dialogClass: 'fororderspage',
				buttons: [
					{
						html: "<i class='icon-save bigger-110'></i>&nbsp; Save",
						"class" : "btn btn-success btn-xs",
						click: function() {	
							if($("#neworderitemmodtable tr").length >= 1){
									saveOrderItems(true);
									reloadTable();
							}
							else{
								alert("Please add items first");
							}
						}
					}
					,
					{
						html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
						"class" : "btn btn-xs",
						click: function() {
							 if (confirm("Are you sure you want to cancel adding items?") == true) {
									$("#addorderdialog")
									.find("input,select")
									.val('')
									.end();
									 var inc = $("#neworderitemmodtable tr").length;
									 for(var i = 0; i <= inc; i++){
										//removeItemInAddModal(i);
										removeItemInAddModalAll(i);
									 }
									 $( this ).dialog( "close" );
									 $("#neworderitemstotal").val('');
									   
								} else {				
								}                 
						}
					}
				],
				create: function( event, ui ) {
					// Set maxWidth
					$(this).css("maxWidth", "460px");
				}
		});
		
		$("#addItemsInCurrentOrder").removeClass('hide').dialog({
			resizable: false,
			width: 'auto',
			modal: true,
			autoOpen: false,
			position: { my: 'top', at: 'top+100' },
			title: "Add Items in order",
			title_html: true,
			dialogClass: 'fororderspage',
			buttons: [
					{
						html: "<i class='icon-save bigger-110'></i>&nbsp; Save",
						"class" : "btn btn-success btn-xs",
						click: function() {	
							var my_data = $("#addItemsInCurrentOrder").data('param_1');
							if($("#addItemsInCurrentOrdermodtable tr").length >= 1){
									savenewitemtoorder(my_data);
									reloadTable();
							}
							else{
								alert("Please add items first");
							}	
						}
					}
					,
					{
						html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
						"class" : "btn btn-xs",
						click: function() {
							  if (confirm("Are you sure you want to cancel adding items?") == true) {
									$("#addItemsInCurrentOrder")
									.find("input,select")
									.val('')
									.end();
									 var inc = $("#addItemsInCurrentOrdermodtable tr").length;
									 for(var i = 0; i < inc; i++){
										removeItemInAddModalAllForItem(i);
									 }
									 $( this ).dialog( "close" );
									 $("#newaddorderitemstotal").val('');		   
							  }             
						}
					}
				],
				create: function( event, ui ) {
					// Set maxWidth
					$(this).css("maxWidth", "460px");
				}
			
		});
		$( "#orderdatadialog" ).removeClass('hide').dialog({
					resizable: false,
					width: 'auto',
					maxWidth: '700px',
					modal: true,
					autoOpen: false,
					title: "Items",
					title_html: true,
					dialogClass: 'fororderspage',
					buttons: [
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Close",
							"class" : "btn btn-xs",
							click: function() {
								$( this ).dialog( "close" );		 
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "700px");
					}
		});
		
		$( "#editorderitemsdialog" ).removeClass('hide').addClass('editorderitems').dialog({
					resizable: false,
					width: 'auto',
					modal: true,
					autoOpen: false,
					title: "Edit Item",
					title_html: true,
					dialogClass: 'fororderspage',
					buttons: [
						{
							html: "<i class='icon-save bigger-110'></i>&nbsp; Update",
							"class" : "btn btn-success btn-xs",
							click: function() {
								saveOrderItems(false);
							}
						}
						,
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
							"class" : "btn btn-xs",
							click: function() {
								$( this ).dialog( "close" );
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "460px");
					}
	        });
		$('#addorderdialog select#neworderitemsupplier').on('change',function(){

					$("#neworderitemmodtable").empty();
					var supplierVal = $(this).val();	
				
					if(supplierVal!=0){	

						result = WPOS.sendJsonData("items/find", JSON.stringify(supplierVal));
						$onebyone =0;
						var table ="neworderitemmodtable";	
						var productfield="neworderitemproduct";	
						addItemModifier(result,$onebyone,table,productfield);	
					}
		})
		$("#newaddorderitemproduct").keyup(function(e){
			$("#suggestion-order-box-items ul").empty();
			var leng = $("#newaddorderitemproduct").val().length;
			var table ="addItemsInCurrentOrdermodtable";	
			var productfield="newaddorderitemproduct";
			var finder = {};
				if(leng >=4){
					var find =  $("#newaddorderitemproduct").val();	
					if(find == "")
						return true;
					if(find == 0 && find.length ==1)
						return true;
					finder.find=find;
					finder.enter=0;
					
					//result = WPOS.sendJsonData("items/findsugesstions", JSON.stringify(finder));
					result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					var newdata="";
					for(k in result)
					{
						$("#suggestion-order-box-items").show();	
						 if (e.keyCode >= 48 && e.keyCode <= 57) {
							// Number
							var findvalue = result[k].code;
							//alert('number');
					
						} else if (e.keyCode >= 65 && e.keyCode <= 90) {
							// Alphabet upper case
							var findvalue = result[k].name;
							//alert("UPPER CASE");
							
						} else if (e.keyCode >= 97 && e.keyCode <= 122) {
							// Alphabet lower case
							var findvalue = result[k].name;
							//alert("LOWER CASE");
						}
						else if  (e.keyCode == 16) {
							// for shift key
							var findvalue = result[k].name;
						}
						else if (e.keyCode == 08) {
							//for backspace
							if (find.match(/[a-z]/i)) {
								// alphabet letters found
								var findvalue = result[k].name;
							}
							else{
								var findvalue = result[k].code;
							}		
						}
						else{
							var findvalue = result[k].code;
						}
						var findvaluecode=result[k].code;
						if(e.keyCode!= 32){
							var ulli = '<li  onClick="return getSpecificItem(\''+findvaluecode+'\',\''+table+'\',\''+productfield+'\');">'+findvalue+'</li>';
						} 
						$("#suggestion-order-box-items ul").append(ulli);
					}	 
				}
				// when enter is pressed	
				if(e.keyCode === 13){	
						
					//return false;
					var find =  $("#newaddorderitemproduct").val();
		
					if(find == "")
					return true;
					var inc = $("#addItemsInCurrentOrdermodtable tr").length;		
					
					for(var i=0; i < inc; i++){
						
						var existcode = $("#addItemsInCurrentOrdermodtableneworderitemcode"+i+"").val();					
						var existname = $("#addItemsInCurrentOrdermodtableneworderitemname"+i+"").val();	
						if(existname == find || existcode == find){
							alert("The item is already added");
							return true;
							//break;
						}												
					}
						$onebyone = 1;
						finder.find=find;
						finder.enter=1;		
						var table ="addItemsInCurrentOrdermodtable";	
						var productfield="newaddorderitemproduct";							
					  result = WPOS.sendJsonData("items/findsugesstions", JSON.stringify(finder));
					  //result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					  
					if(result != false){
						   addItemModifier(result,$onebyone,table,productfield);	
					}
					else{
						if (find.match(/[a-z]/i)) {
							// alphabet letters found
							alert("Item not found, please enter barcode to add as new product.");
							$("#newaddorderitemproduct").val("");
							$("#newaddorderitemproduct").focus();
						}
						else{
							alert("Item with this barcode does not exist and now it will be added.");
							addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[] ,"reorder_value":"1","maincost":"0.00","currentquantity":"0"}},$onebyone,table,productfield);
						}
					}								
				}
		});
		$("#neworderitemproduct").keyup(function(e){
				/*******
				*
				*
				* 12 June 2017 Dynamic suggestion list
				*
				*
				********/
				
				$("#suggestion-order-box ul").empty();
				var leng = $("#neworderitemproduct").val().length;
				var table ="neworderitemmodtable";	
				var productfield="neworderitemproduct";		
				var finder = {};
				if(leng >=4){
					var find =  $("#neworderitemproduct").val();	
					if(find == "")
						return true;
					if(find == 0 && find.length ==1)
						return true;
					finder.find=find;
					finder.enter=0;
					
					//result = WPOS.sendJsonData("items/findsugesstions", JSON.stringify(finder));
					result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					var newdata="";
					for(k in result)
					{
						$("#suggestion-order-box").show();	
						 if (e.keyCode >= 48 && e.keyCode <= 57) {
							// Number
							var findvalue = result[k].code;
							//alert('number');
					
						} else if (e.keyCode >= 65 && e.keyCode <= 90) {
							// Alphabet upper case
							var findvalue = result[k].name;
							//alert("UPPER CASE");
							
						} else if (e.keyCode >= 97 && e.keyCode <= 122) {
							// Alphabet lower case
							var findvalue = result[k].name;
							//alert("LOWER CASE");
						}
						else if  (e.keyCode == 16) {
							// for shift key
							var findvalue = result[k].name;
						}
						else if (e.keyCode == 08) {
							//for backspace
							if (find.match(/[a-z]/i)) {
								// alphabet letters found
								var findvalue = result[k].name;
							}
							else{
								var findvalue = result[k].code;
							}		
						}
						else{
							var findvalue = result[k].code;
						}
						var findvaluecode=result[k].code;
						if(e.keyCode!= 32){
							var ulli = '<li  onClick="return getSpecificItem(\''+findvaluecode+'\',\''+table+'\',\''+productfield+'\');">'+findvalue+'</li>';
						} 
						$("#suggestion-order-box ul").append(ulli);
					}	 
				}
				// when enter is pressed	
				if(e.keyCode === 13){	
						
					//return false;
					var find =  $("#neworderitemproduct").val();
		
					if(find == "")
					return true;
					var inc = $("#neworderitemmodtable tr").length;		
					
					for(var i=0; i < inc; i++){
						
						var existcode = $("#neworderitemmodtableneworderitemcode"+i+"").val();					
						var existname = $("#neworderitemmodtableneworderitemname"+i+"").val();	
						if(existname == find || existcode == find){
							alert("The item is already added");
							return true;
							//break;
						}												
					}
						$onebyone = 1;
						finder.find=find;
						finder.enter=1;		
						var table ="neworderitemmodtable";	
						var productfield="neworderitemproduct";							
					  result = WPOS.sendJsonData("items/findsugesstions", JSON.stringify(finder));
					  //result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					
					if(result != false){
						   addItemModifier(result,$onebyone,table,productfield);	
					}
					else{
						if (find.match(/[a-z]/i)) {
							// alphabet letters found
							alert("Item not found, please enter barcode to add as new product.");
							$("#neworderitemproduct").val("");
							$("#neworderitemproduct").focus();
						}
						else{
							alert("Item with this barcode does not exist and now it will be added.");
							addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[] ,"reorder_value":"1","maincost":"0.00","currentquantity":"0"}},$onebyone,table,productfield);
						}
					}								
				}
		});
		
		/*****
		*
		* Get data of a specific code or name
		*
		*****/
		function getSpecificItem(code,table,productfield){
			$("#suggestion-order-box").hide();
			$("#suggestion-order-box-items").hide();
				var inc = $("#"+table+" tr").length;		
				for(var i=0; i <= inc; i++){
					var existcode = $("#"+table+"neworderitemcode"+i+"").val();
					if(existcode == code.toUpperCase()){
						alert("The item is already added");
						return true;
					}												
				}
				var finder={};
				finder.find=code;
				finder.enter=1;		

				result1 = WPOS.sendJsonData("items/findsugesstions", JSON.stringify(finder));
				if(result1==false){
					result2 = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					result =result2;
				}
				else{
					result =result1;
				}
			
				if(result != false){
					$onebyone= 1;
					addItemModifier(result ,$onebyone,table,productfield);			
				}
				else{
					addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[] ,"reorder_value":"1","maincost":"0.00","currentquantity":"0"}},$onebyone,table,productfield);
				}			
		}
		//function for adding items based on supplier id
	    function addItemModifier(itemData,$onebyone,table,productfield){
			var cat;
			if($onebyone == 1)	{
				var inc = $("#"+table+" tr").length;
				counter = inc;
			}
			else{
				var counter=0;
			}
			
				 if(table == "neworderitemmodtable"){
					 var totalfield = "neworderitemstotal";	
				 }
				 else if(table == "addItemsInCurrentOrdermodtable"){
					 var totalfield = "newaddorderitemstotal";	
				 }
				
				 for (var i in itemData){
					 if(itemData[i].maincost == null || itemData[i].currentquantity == null){	
						 itemData[i].maincost=0;
						 itemData[i].currentquantity=0;
					 }
					 if(itemData[i].price == null){
						 itemData[i].price=0;
					 }
					 		
					 $("#"+table).append('<tr class="item"><td><input style="width : 90px" type="text" class="modname itemval" name="neworderitemcode"  id="'+table+'neworderitemcode'+counter+'" value="'+itemData[i].code+'" readonly="true"/><input id="'+table+'neworderitemstatus'+counter+'" type="hidden" value="0"></td><td><input type="text" class="modname itemval nameindialog" name="neworderitemname"  id="'+table+'neworderitemname'+counter+'" value="'+itemData[i].name+'"/></td><td><select id="'+table+'neworderitemcategory'+counter+'" class="catselect"></select></td><td><input type="text" style="width: 60px" class="modminqty itemval" min="1" readonly name="neworderitemqty" id="'+table+'neworderitemqty'+counter+'" value="'+itemData[i].currentquantity+'"/></td><td><input type="number" min=1 onmouseup="reordermouseUp('+counter+',\''+table+'\',\''+totalfield+'\')" name="reorderqty" class="reorderqtycls" id="'+table+'newreorderqty'+counter+'" value="'+itemData[i].reorder_value+'"></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="neworderitemcost" onkeyup="reordermouseUp('+counter+',\''+table+'\',\''+totalfield+'\')" id="'+table+'neworderitemcost'+counter+'" value="'+itemData[i].maincost+'"/></td><td><input type="text" style="width: 70px" onkeyup="reordermouseUp('+counter+',\''+table+'\',\''+totalfield+'\')" name="neworderitemprice" id="'+table+'neworderitemprice'+counter+'" class="modprice itemval" value="'+itemData[i].price+'"/></td><td><input type="text" style="width: 70px" name="neworderitemmargin" id="'+table+'neworderitemmargin'+counter+'" class="modminqty itemval" value="0"/></td><td><input type="text" name="neworderitemsum" id="'+table+'neworderitemsum'+counter+'" class="modminqty itemval totaldialog" value="'+itemData[i].total+'" readonly/></td><td style="text-align: right;"><button id="'+table+'removeadditem'+counter+'" class="btn btn-danger btn-xs" onclick="removeItemInAddModal('+counter+',\''+table+'\',\''+totalfield+'\')">X</button></td></tr>');
			         
			         cat = itemData[i].categoryid;	
			         
			         //calculate margin and append it into margin field 
			         
			         calculationMarginTotal(counter,table,totalfield);
			         
			         //append categories into category select box
					 var catsel = $("#"+table+"neworderitemcategory"+counter);
					 catsel.html('');
					 catsel.append('<option class="catid-0" value="0">None</option>');
					 for (key in categories){
							catsel.append('<option class="catid-'+categories[key].id+'" value="'+categories[key].id+'">'+categories[key].name+'</option>');
					 }
					 if(cat)
					 $("#"+table+"neworderitemcategory"+counter).val(cat);
			         
			         counter++;
				}
			       $("#"+productfield).val("");		
			 
	    }
	   	
	    //function for calculating total margin
	    function calculationMarginTotal(inc,table,totalfield){	
			
			  var cost = $("#"+table+"neworderitemcost"+inc).val();
			  var price = $("#"+table+"neworderitemprice"+inc).val();
			  var qty = $("#"+table+"newreorderqty"+inc).val();	
		
			  CostQty = (Number(cost)*Number(qty)).toFixed(2);
			  
			  $("#"+table+"neworderitemsum"+inc).val(CostQty);   			   					   			    
				   if(cost != "" && price!= "" ){				   
					   margin = ((Number(price)-Number(cost))/Number(price)) * 100; 
					   margin = margin.toFixed(2); 					
							if(margin == "-Infinity" || isNaN(margin)){		
								$('#'+table+'neworderitemmargin'+inc).val(0);	
							}
							else{
								$('#'+table+'neworderitemmargin'+inc).val(margin+"%"); 
							}
				   }else{
					   
					   $("#"+table+"neworderitemmargin"+inc).val(0);
				   }	 		
				   	   
			 var invoice_total = 0;
			 /*
			 var rows = $("#neworderitemmodtable tr").length;
			    for(var i = 0; i < rows; i++){
					var val=$("#neworderitemsum"+i).val();
					invoice_total = Number(invoice_total) + Number(val);
				}			
				invoice_total = invoice_total.toFixed(2) 
				$("#neworderitemstotal").val(invoice_total);    
				
				*/
				
				$("#"+table+" tr").each(function(i, obj) {
					//test
					var td = $(this).find("td:first input").attr('id');
				
					var res = td.split(table+"neworderitemcode");
					var k = res[1];	
					var val=$("#"+table+"neworderitemsum"+k).val();
					invoice_total = Number(invoice_total) + Number(val);
				 });        
				invoice_total = invoice_total.toFixed(2) 	
				$("#"+totalfield).val(invoice_total);    
		}
		//function for removing items in modal when pressed cancel
	    function removeItemInAddModal(val,table,totalfield){
			var sum = $("#"+table+"neworderitemsum"+val+"").val();
			$("#"+table+"removeadditem"+val+"").parent().parent().remove();
			var total = $("#"+totalfield).val(); 
			var newtotal = total-sum;
			newtotal = newtotal.toFixed(2);
			$("#"+totalfield).val(newtotal);			
			$("#"+table+" tr").each(function(i, obj) {
					$(this).find('td').each (function() {
					  // do your cool stuff
						var td = $(this).find("input").attr('id');	
						var tdselect = $(this).find("select").attr('id');	
						var tdbutton = $(this).find("button").attr('id');	
					
						if(td){
							var res = td.replace(/\d+/g, '');
							var findtype = 'input';
							switch(res){
								case table+'newreorderqty' :
									$(this).find('input').attr('onmouseup','reordermouseUp(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
									break;
								case table+'neworderitemcost':
									$(this).find('input').attr('onkeyup','reordermouseUp(\''+i+'\',\''+table+'\',\''+totalfield+'\')');			
									break;
								case table+'neworderitemprice':
									$(this).find('input').attr('onkeyup','reordermouseUp(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
									break;
							}
						}
						else if(tdselect){
							var res = tdselect.replace(/\d+/g, '');	
							var findtype = 'select';
						}
						else if(tdbutton){
							var res = tdbutton.replace(/\d+/g, '');
							var findtype = 'button';
							$(this).find('button').attr('onclick','removeItemInAddModal(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
						}
						$(this).find(findtype).attr('id',res+i);
					});    
			});   
		}
		//function for removing everything when pressed cancel
		function removeItemInAddModalAll(val){
			var sum = $("#neworderitemmodtableneworderitemsum"+val+"").val();
			$("#neworderitemmodtableremoveadditem"+val+"").parent().parent().remove();
			var total = $("#neworderitemstotal").val(); 
			var newtotal = total-sum;
			 newtotal = newtotal.toFixed(2);
			 $("#neworderitemstotal").val(newtotal);			
		}
		function removeItemInAddModalAllForItem(val){
			var sum = $("#addItemsInCurrentOrdermodtableneworderitemsum"+val+"").val();
			$("#addItemsInCurrentOrdermodtableremoveadditem"+val+"").parent().parent().remove();
			var total = $("#newaddorderitemstotal").val(); 
			var newtotal = total-sum;
			 newtotal = newtotal.toFixed(2);
			 $("#newaddorderitemstotal").val(newtotal);			
		}
		//function to save the orders and its items
		function saveOrderItems(isnewitem){

	        // show loader
	        WPOS.util.showLoader();
	        var order = { 
				product : []
				 };
	        var result,costval;
			var itemEdit = {};
	        if (isnewitem){
				
				var rows = $("#neworderitemmodtable tr").length;
				var i=0;
	            // adding a new item    
	           
	            order.supplierid = $("#neworderitemsupplier").val();
	            order.order_number = Math.floor(Math.random()*90000) + 10000;
	            order.totals_sum = $("#neworderitemstotal").val();
	            order.created = $("#neworderitemdate").val();
				
				
	           for(i = 0; i < rows; i++){
				   var test = {}
				    test.status = $("#neworderitemmodtableneworderitemstatus"+i).val();
				    test.code = $("#neworderitemmodtableneworderitemcode"+i).val();
					test.name = $("#neworderitemmodtableneworderitemname"+i).val();
					test.categoryid = $("#neworderitemmodtableneworderitemcategory"+i).val();
					test.qty = $("#neworderitemmodtableneworderitemqty"+i).val();
					test.reorderqty = $("#neworderitemmodtablenewreorderqty"+i).val();
					costval = $("#neworderitemmodtableneworderitemcost"+i).val();
					test.cost = (costval ? costval : 0);
					test.price = $("#neworderitemmodtableneworderitemprice"+i).val();
					test.margin = $("#neworderitemmodtableneworderitemmargin"+i).val();
					test.total = $("#neworderitemmodtableneworderitemsum"+i).val();
					order['product'].push(test);
			   }           
			   
	          result = WPOS.sendJsonData("orders/add", JSON.stringify(order));
	          $("#neworderitemmodtable").empty();
	          $("#neworderitemstotal").val('');        
	          $("#addorderdialog").dialog( "close" );
	            WPOS.util.hideLoader();
	        }
	        else{
				// updating an item
	            itemEdit.id = $("#ordersitemsid").val();
	            itemEdit.orderid = $("#ordersitemsorderid").val();
	            itemEdit.itemcode = $("#ordersitemsitemcode").val();
	            itemEdit.itemname = $("#ordersitemsitemname").val();
	            itemEdit.itemcost = $("#ordersitemsitemcost").val();
	            itemEdit.itemprice = $("#ordersitemsitemprice").val();
	            itemEdit.itemreorderqty = $("#ordersitemsreorderqty").val();
  
	            result = WPOS.sendJsonData("order/editOrderitems", JSON.stringify(itemEdit));
	            
	            var order_number = $('#ordersitemordernumber').val();
	            var order_supplier = $('#ordersitemsordersupplier').val();
	            var order_date = $('#ordersitemsorderdate').val();
	            if (result!==false){
					orderItemData = WPOS.sendJsonData("orders/getItemsandorder", JSON.stringify({orderid: $("#ordersitemsorderid").val()}));
					for(i in orderItemData){
					 datte = orderItemData[i].order_date;
					 order_total = orderItemData[i].order_total;
					 
					}
					getOrderItemsOfNumber($("#ordersitemsorderid").val(),order_number,order_total,order_supplier,datte);
					reloadTable();
	                $("#editorderitemsdialog").dialog("close");
	            }
			}
	        // hide loader
	        WPOS.util.hideLoader();
	    }
	    
	    //function to add new item to the order
	    function savenewitemtoorder(my_data){
			 // show loader
	        WPOS.util.showLoader();
	        var order = { 
				product : []
				 };
	        var result,costval;
			var itemEdit = {};
			var rows = $("#addItemsInCurrentOrdermodtable tr").length;
				var i=0;
	            // adding a new item    
	            var now = new Date();
				var month = now.getMonth()+1;
				if(month<10) {
						month='0'+month
				}
				var day = now.getDate();
				if(day<10) {
					day='0'+day
				}
				var date = month+'/'+day+'/'+now.getFullYear(); 
	            order.supplierid = $("#newaddorderitemsupplier").val();
	            order.order_number = my_data;
	            order.totals_sum = $("#newaddorderitemstotal").val();
	            order.created = date;
				
				
	           for(i = 0; i < rows; i++){
				   var test = {}
				    test.status = $("#addItemsInCurrentOrdermodtableneworderitemstatus"+i).val();
				    test.code = $("#addItemsInCurrentOrdermodtableneworderitemcode"+i).val(); 
					test.name = $("#addItemsInCurrentOrdermodtableneworderitemname"+i).val();
					test.categoryid = $("#addItemsInCurrentOrdermodtableneworderitemcategory"+i).val();
					test.qty = $("#addItemsInCurrentOrdermodtableneworderitemqty"+i).val();
					test.reorderqty = $("#addItemsInCurrentOrdermodtablenewreorderqty"+i).val();
					costval = $("#addItemsInCurrentOrdermodtableneworderitemcost"+i).val();
					test.cost = (costval ? costval : 0);
					test.price = $("#addItemsInCurrentOrdermodtableneworderitemprice"+i).val();
					test.margin = $("#addItemsInCurrentOrdermodtableneworderitemmargin"+i).val();
					test.total = $("#addItemsInCurrentOrdermodtableneworderitemsum"+i).val();
					order['product'].push(test);
			   }           
			   
	          result = WPOS.sendJsonData("orders/addNewItemToOrder", JSON.stringify(order));
	          $("#addItemsInCurrentOrdermodtable").empty();
	          $("#newaddorderitemstotal").val('');        
	          $("#addItemsInCurrentOrder").dialog( "close" );
			// hide loader
	        WPOS.util.hideLoader();
		}
	    //function to reload the datatable and load content
	    function reloadTable(){
			newaddedorder = WPOS.sendJsonData("orders/getGroupedData", JSON.stringify({"orders/getGroupedData":""}));
			var allorders = [];
			var temporder;
			for (var key in newaddedorder){
				temporder = newaddedorder[key];
				allorders.push(temporder);
			}
			datatable.fnClearTable(false);
			datatable.fnAddData(allorders, false);
			datatable.api().draw(false);
		}
		//get all items of a particular id and shows item in a datatable   
		function getOrderItemsOfNumber(number,order_number,order_total,suppliername,orderdate){
			
			WPOS.util.showLoader();
			orderItemData = WPOS.sendJsonData("orders/getItems", JSON.stringify({orderid: number}));
			
			//==================================================================================//
					var orderitemarray1 = [];
					var tempitemreceive1;
					var taxrules = WPOS.getTaxTable().rules;
					for (var key in orderItemData){	
						tempitemreceive1 = orderItemData[key];
						orderitemarray1.push(tempitemreceive1);	
					}
	
					datatables = $('#detailOrderData').dataTable({
						"destroy": true,
						"bProcessing": true,
						"aaData": orderitemarray1,
						"aoColumns": [
							{ "mData":"id" },
							{ "mData":"order_id" },
							{ "mData":"item_code" },
							{ "mData":"item_name"},
							{ "mData":function(data,type,val){return (data['item_cost']==""?"":WPOS.util.currencyFormat(data["item_cost"]));} },
							{ "mData":function(data,type,val){return (data['item_price']==""?"":WPOS.util.currencyFormat(data["item_price"]));} },
							{ "mData":"item_reorder_quantity" },
							{ mData:function(data,type,val){return '<div class="action-buttons"><a class="green" onclick="editorder($(this).closest(\'tr\').find(\'td\').eq(0).text());"><i class="icon-pencil bigger-130"></i><a class="red" onclick="deleteorderitem($(this).closest(\'tr\').find(\'td\').eq(0).text());"><i class="icon-trash bigger-130"></i></a></div>'; }, "bSortable": false }
						],
						"columns": [
							{type:  "numeric"},
							{type:  "numeric"},
							{type:  "numeric"},
							{type:  "string"},
							{type:  "string"},
							{type:  "string"},
							{type:  "numeric"},
						]	
					});	
			//=================================================================================//		
			
			WPOS.util.hideLoader();  
			$('#mainorderrow').remove();
		    $('#orderdatadialog').append('<div id="mainorderrow" class="mainrowclass"><table  class="table"><thead><tr><th>ID</th><th>Order Number</th><th>Order Total</th><th>Supplier</th><th>Order Date</th></tr></thead>'+'<tr><td>'+number+'</td><td>'+order_number+'</td><td>'+order_total+'</td><td>'+suppliername+'</td><td>'+orderdate+'</td></tr></table></div>');
			$("#orderdatadialog").dialog('open');
		}
		//remove order using order id
		function removeOrder(number){
			var answer = confirm("Are you sure you want to delete this order?");
	        if (answer){
	            // show loader
	            WPOS.util.hideLoader();
	          
	            if (WPOS.sendJsonData("orders/deleteOrder", JSON.stringify({orderid: number}))){
	               reloadTable();
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
		}
		//edit order 
		function editorder(id){
	
				for(var  i in orderItemData){
					if(id == orderItemData[i].id){
						 var item = orderItemData[i]; 
					}
				}
			    
	          $("#ordersitemsid").val(item.id);
	          $("#ordersitemsorderid").val(item.order_id);
	          $("#ordersitemsitemcode").val(item.item_code);
	          $("#ordersitemsitemname").val(item.item_name);
	          $("#ordersitemsitemcost").val(item.item_cost);
	          $("#ordersitemsitemprice").val(item.item_price);
	          $("#ordersitemsreorderqty").val(item.item_reorder_quantity);
				
			  $('#ordersitemordernumber').val($("#mainorderrow table tr").find('td').eq(1).text());
			  $('#ordersitemordertotal').val($("#mainorderrow table tr").find('td').eq(2).text());
			  $('#ordersitemsordersupplier').val($("#mainorderrow table tr").find('td').eq(3).text());
			  $('#ordersitemsorderdate').val($("#mainorderrow table tr").find('td').eq(4).text());
	        $("#editorderitemsdialog").dialog("open");
			
		}
		//delete specific item exists in a order
		function deleteorderitem(id){
			for(var  i in orderItemData){
					if(id == orderItemData[i].id){
						 var item = orderItemData[i];
						 var order_id= item.order_id;
					}
			}	
			
			var answer = confirm("Are you sure you want to delete this item?");
	        if (answer){
	            // show loader
	            WPOS.util.hideLoader();
	          
	            if (WPOS.sendJsonData("order/deleteorderitem", '{"id":'+id+',"orderid":'+order_id+'}')){
					
					var order_number = $('#ordersitemordernumber').val();
					var order_supplier = $('#ordersitemsordersupplier').val();
					var order_date = $('#ordersitemsorderdate').val();
					orderItemData = WPOS.sendJsonData("orders/getItemsandorder", JSON.stringify({orderid: $("#ordersitemsorderid").val()}));
					for(i in orderItemData){
					 datte = orderItemData[i].order_date;
					 order_total = orderItemData[i].order_total;
					 
					}
					getOrderItemsOfNumber(order_id,order_number,order_total,order_supplier,datte);
	               reloadTable();
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
		}
		//calculate total when cost or reorder quantity textbox are changed
		function reordermouseUp(inc,table,totalfield){
			calculationMarginTotal(inc,table,totalfield)
		}
		//edit the current order , user can delete or add items in current order
		function editCurrentOrder(id){
			WPOS.util.showLoader();
			 $("#addItemsInCurrentOrdermodtable").empty();
	         $("#newaddorderitemstotal").val('');        
			orderItemData = WPOS.sendJsonData("orders/getItemsandorder", JSON.stringify({orderid: id}));
			for(i in orderItemData){
					 datte = orderItemData[i].order_date;
					 supplierid = orderItemData[i].order_supplier_id;
					 
			}
			var date = new Date(datte);
			$("#newaddorderitemdate").val((date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear());
			$("#newaddorderitemsupplier").val(supplierid);   
			addItemModifier(orderItemData,0,"addItemsInCurrentOrdermodtable","newaddorderitemproduct");	
			$("#addItemsInCurrentOrder").data("param_1",id).dialog("open");
			
			 // hide loader
	             WPOS.util.hideLoader();
		}
	</script>
