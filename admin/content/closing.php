<!-- WallacePOS: Copyright (c) 2014 WallaceIT <micwallace@gmx.com> <https://www.gnu.org/licenses/lgpl.html> -->
	<div class="page-header">
	    <h1 style="margin-right: 20px; display: inline-block;">
	        Accounting
	    </h1>
<!--
	      <button onclick="$('#addorderdialog').dialog('open');" id="addbtn" class="btn btn-primary btn-sm pull-right"><i class="icon-pencil align-top bigger-125"></i>Create Order</button>
-->

	</div><!-- /.page-header -->
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-xs-12">	
					<div class="table-header">
						Manage your Closing
					</div>
					<table id="closingtable" class="table table-striped table-bordered table-hover dt-responsive" style="width:100%;">
						<thead>
							<tr>
								<th data-priority="3">ID</th>
								<th data-priority="3">User</th>
								<th data-priority="2">Device/Location</th>
								<th data-priority="5">Date</th>
								<th data-priority="5">Last Updated</th>
								<th data-priority="5">Sales</th>
								<th data-priority="5">Refunds</th>
								<th data-priority="5">Voids</th>
								<th data-priority="5">Takings</th>
								<th data-priority="1">TotalCount</th>
								<th data-priority="1">Balance</th>
								<th class="noexport" data-priority="2"></th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
	<div id="editClosingitemsdialog" class="hide">
	    <div class="tabbable" style="min-width: 360px; min-height: 310px;">
	        <ul class="nav nav-tabs">
	            <li class="active">
	                <a href="#closingdetails" data-toggle="tab">
	                    Details
	                </a>
	            </li>
	        </ul>
	        <div class="tab-content" style="min-height: 320px;">
	            <div class="tab-pane active in" id="closingitemdetails">              
	                <table>
						<input id="closingid" type="hidden"/>
						<input id="closinguser" type="hidden"/>
						<tr>
							<td style="text-align: right;"><label>User:&nbsp;</label></td>
							<td><input id="closingusername" type="text" readonly="true"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Device/Location:&nbsp;</label></td>
							<td><input id="closingloc" type="text" readonly="true"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Date:&nbsp;</label></td>
							<td><input id="closingdate" readonly="true" type="text"/><br/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Sales:&nbsp;</label></td>
							<td><input id="closingsales" type="text" value="" /></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Refunds:&nbsp;</label></td>
							<td><input id="closingrefund" type="text" value=""/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Voids:&nbsp;</label></td>
							<td><input id="closingvoid"  type="text" value=""/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Takings:&nbsp;</label></td>
							<td><input id="closingtaking" type="text" value=""/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>TotalCount:&nbsp;</label></td>
							<td><input id="closingtotalcount" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Balance:&nbsp;</label></td>
							<td><input id="closingbalance" type="text"/></td>
						</tr>	                
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
	
	<script>
		var allclosing = null;
	    
		var datatable,datatables;
		$(document).ready(function(){
			var data1 = WPOS.sendJsonData("accounting/closing", JSON.stringify({"accounting/closing":""}));

	        var itemarray = [];
			var tempitem;
			for (var key in data1){
				tempitem = data1[key];
				tempitem.devlocname = (WPOS.devices.hasOwnProperty(tempitem.deviceid)?WPOS.devices[tempitem.deviceid].name:'NA')+" / "+(WPOS.locations.hasOwnProperty(tempitem.locationid)?WPOS.locations[tempitem.locationid].name:'NA');
				itemarray.push(tempitem);
			}
	        
	        
	        datatable = $('#closingtable').dataTable({
	            "bProcessing": true,
	            "aaData": itemarray,
	            "aoColumns": [
	                { "mData":"id" },
	                { "mData":function(data, type, val){ var users = WPOS.getConfigTable().users; if (users.hasOwnProperty(data.userid)){ return users[data.userid].username; } return 'N/A'; } },
	                { "mData":"devlocname" },
	                { "mData":"timeStamp" },
	                { "mData":"updated_at" },
	                { "mData":function(data,type,val){return (data["sales"]<0)?'-'+'$'+ Math.abs(data["sales"]):'$'+Math.abs(data["sales"]);}},
	                { "mData":function(data,type,val){return (data["refunds"]<0)?'-'+'$'+ Math.abs(data["refunds"]):'$'+Math.abs(data["refunds"]);}},
	                { "mData":function(data,type,val){return (data["voids"]<0)?'-'+'$'+ Math.abs(data["voids"]):'$'+Math.abs(data["voids"]);}},
	                { "mData":function(data,type,val){return (data["takings"]<0)?'-'+'$'+ Math.abs(data["takings"]):'$'+Math.abs(data["takings"]);}},
	                { "mData":function(data,type,val){return (data["totalcount"]<0)?'-'+'$'+ Math.abs(data["totalcount"]):'$'+Math.abs(data["totalcount"]);}},
	                { "mData":function(data,type,val){return (data["balance"]<0)?'-'+'$'+ Math.abs(data["balance"]):'$'+Math.abs(data["balance"]);}},
	                { mData:function(data,type,val){ return '<div class="action-buttons"><a class="red" onclick=removeClosing("'+data.id+'");><i class="icon-trash bigger-130"></i></a><a class="green" onclick="openClosingEditDialog($(this).closest(\'tr\').find(\'td\').eq(0).text());"><i class="icon-pencil bigger-130"></i></a></div>'; }, "bSortable": false },
	            ],   
	            "columns": [
	                {type: "numeric"},
	                {type: "string"},
	                {type: "string"},
	                {type: "string"},
	                {type: "string"},
	                {type: "currency"},
	                {type: "currency"},
	                {type: "currency"},
	                {type: "currency"},
	                {type: "currency"},
	                {type: "currency"},
	                {}
	            ]
	        });
			  // hide loader
			  WPOS.util.hideLoader();
		});
		$( "#editClosingitemsdialog" ).removeClass('hide').addClass('editclosingitems').dialog({
					resizable: false,
					width: 'auto',
					modal: true,
					autoOpen: false,
					title: "Edit Closing",
					title_html: true,
					dialogClass: 'fororderspage',
					buttons: [
						{
							html: "<i class='icon-save bigger-110'></i>&nbsp; Update",
							"class" : "btn btn-success btn-xs",
							click: function() {
								saveClosing();
							}
						}
						,
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
							"class" : "btn btn-xs",
							click: function() {
								$( this ).dialog( "close" );
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "460px");
					}
	    });
		function removeClosing(id){
			var answer = confirm("Are you sure you want to delete this closing?");
	        if (answer){
	            // show loader
	            WPOS.util.showLoader();
	          
	            if (WPOS.sendJsonData("accounting/deleteClosing", JSON.stringify({closingid: id}))){
	               reloadTable();
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
		}
		function openClosingEditDialog(id){
				var closingd = WPOS.sendJsonData("accounting/getclosing", JSON.stringify({closingid: id}));
		      
	            WPOS.util.showLoader();
				for(var  i in closingd){
					if(id == closingd[i].id){
						 var item = closingd[i]; 
						 var devlocname = (WPOS.devices.hasOwnProperty(closingd[i].deviceid)?WPOS.devices[closingd[i].deviceid].name:'NA')+" / "+(WPOS.locations.hasOwnProperty(closingd[i].locationid)?WPOS.locations[closingd[i].locationid].name:'NA');
					}
				}
				
			   var users = WPOS.getConfigTable().users;
			   if (users.hasOwnProperty(item.userid)){
				    var username =  users[item.userid].username; 
			   }
			   
				  $("#closingid").val(item.id);
				  $("#closinguser").val(item.userid); 
				  $("#closingusername").val(username); 
				  $("#closingloc").val(devlocname); 
				  $("#closingdate").val(item.timeStamp);
				  $("#closingsales").val(item.sales);
				  $("#closingrefund").val(item.refunds);
				  $("#closingvoid").val(item.voids);
				  $("#closingtaking").val(item.takings);
				  $("#closingtotalcount").val(item.totalcount);
				  $("#closingbalance").val(item.balance);
				  
					
				$("#editClosingitemsdialog").dialog("open");
				
	            // hide loader
	            WPOS.util.hideLoader();
	        
			
		}
		function saveClosing(){
		    var item = {};
		    item.id = $("#closingid").val();
		    item.timeStamp = $("#closingdate").val();
		    item.sales = $("#closingsales").val();
		    item.refunds =  $("#closingrefund").val();
		    item.voids = $("#closingvoid").val();
		    item.takings = $("#closingtaking").val();
		    item.totalcount = $("#closingtotalcount").val();
		    item.balance = $("#closingbalance").val();
		    
		    var config = WPOS.getConfigTable();
		    var users = WPOS.getConfigTable().users; 
	
		    if (users.hasOwnProperty($("#closinguser").val())){ 
				result = WPOS.sendJsonData("accounting/closingedit", JSON.stringify(item));
			}
			else{
				alert("No user found");
			}
			reloadTable();
			$("#editClosingitemsdialog").dialog("close");
			WPOS.util.hideLoader();
		}
		function reloadTable(){
			var data1 = WPOS.sendJsonData("accounting/closing", JSON.stringify({"accounting/closing":""}));
			
	        var itemarray = [];
			var tempitem;
			for (var key in data1){
				tempitem = data1[key];
				tempitem.devlocname = (WPOS.devices.hasOwnProperty(tempitem.deviceid)?WPOS.devices[tempitem.deviceid].name:'NA')+" / "+(WPOS.locations.hasOwnProperty(tempitem.locationid)?WPOS.locations[tempitem.locationid].name:'NA');
				itemarray.push(tempitem);
			}
			datatable.fnClearTable(false);
			datatable.fnAddData(itemarray, false);
			datatable.api().draw(false);
		}
	</script>
