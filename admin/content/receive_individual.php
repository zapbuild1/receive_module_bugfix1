	<style>
		.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons.ui-draggable, .ui-dialog-content.ui-widget-content{
	    max-width: 960px !important;
	    width: 100% !important;
	}
	#addreceivedialog label{width:120px;}
	#addreceivedialog .input-width{width:150px;}
	#addreceivedialog input{width:100%;}
	#addreceivedialog .supselect {
	    width: 150px;
	}
	.input-width-in {
	    width: 70% !important;
	}

	</style>
	<!-- WallacePOS: Copyright (c) 2014 WallaceIT <micwallace@gmx.com> <https://www.gnu.org/licenses/lgpl.html> -->
	<div class="page-header">
	    <h1 style="margin-right: 20px; display: inline-block;">
	        Receive
	    </h1>
	    <button onclick="$('#itemmodtable').empty();$('#addreceivedialog').dialog('open');" id="addbtn" class="btn btn-primary btn-sm pull-right"><i class="icon-pencil align-top bigger-125"></i>Receive Items</button>
	   <!-- <button class="btn btn-success btn-sm pull-right" style="margin-right: 10px;" onclick="exportItems();"><i class="icon-cloud-download align-top bigger-125"></i>Export CSV</button>
	    <button class="btn btn-success btn-sm pull-right" style="margin-right: 10px;" onclick="openImportDialog();"><i class="icon-cloud-upload align-top bigger-125"></i>Import CSV</button>-->
	</div><!-- /.page-header -->

	<div class="row">
	<div class="col-xs-12">
	<!-- PAGE CONTENT BEGINS -->

	<div class="row">
	<div class="col-xs-12">

	<div class="table-header">
	    Manage your business products
	</div>

	<table id="receiveitemstable" class="table table-striped table-bordered table-hover dt-responsive" style="width:100%;">
	<thead>
	<tr>
	    <th data-priority="0" class="center">
	        <label>
	            <input type="checkbox" class="ace" />
	            <span class="lbl"></span>
	        </label>
	    </th>
	  <th data-priority="1">ID</th>
	    <th data-priority="3">Invoice Number</th>
	    <th data-priority="2">Invoice Total</th>
	  <!--  <th data-priority="7">Tax</th>-->
	    <th data-priority="4">Name</th>
	    <th data-priority="5">Total Cost</th>
	    <th data-priority="6">Total Price</th>
	    <th data-priority="7">Margin</th>
	    <th data-priority="8">Total</th>
	    <th data-priority="9">Category</th>
	    <th data-priority="10">Supplier</th>
	    <th data-priority="11">Date</th>
	    <th class="noexport" data-priority="2"></th>
	</tr>
	</thead>
	<tbody>

	</tbody>
	</table>

	</div>
	</div>

	</div><!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
	<div id="editreceivedialog" class="hide">
	    <div class="tabbable" style="min-width: 360px; min-height: 310px;">
	        <ul class="nav nav-tabs">
	            <li class="active">
	                <a href="#receiveitemdetails" data-toggle="tab">
	                    Details
	                </a>
	            </li>
	           <!-- <li class="">
	                <a href="#receiveitemoptions" data-toggle="tab">
	                    Options
	                </a>
	            </li>-->
	        </ul>
	        <div class="tab-content" style="min-height: 320px;">
	            <div class="tab-pane active in" id="receiveitemdetails">
	                <table>
						<tr>
							<td style="text-align: right;"><label>Supplier:&nbsp;</label></td>
							<td><select id="receiveitemsupplier" class="supselect" style="width: 100% !important;">
								</select>
								<input id="receiveitemid" type="hidden"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Category:&nbsp;</label></td>
							<td><select id="receiveitemcategory" class="catselect" style="width: 100% !important;">
								</select>
								</td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Invoice Number:&nbsp;</label></td>
							<td><input id="receiveitemnumber" type="text" readonly="true"/><br/><input id="receiveitemid" type="hidden"/></td></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Invoice Total:&nbsp;</label></td>
							<td><input id="receiveiteminvoicetotal" type="text"/><br/></td>
						</tr>
						<!--<tr>
							<td style="text-align: right;"><label>Product:&nbsp;</label></td>
							<td><input id="receiveitemproduct" type="text" /></td>
						</tr>-->
						<tr>
							<td style="text-align: right;"><label>Stock Code:&nbsp;</label></td>
							<td><input id="receiveitemcode" type="text" value="0" readonly="true"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Name:&nbsp;</label></td>
							<td><input id="receiveitemname" type="text" value="0"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Qty:&nbsp;</label></td>
							<td><input id="receiveitemqty"  type="number" min="1" value="1"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Cost:&nbsp;</label></td>
							<td><input id="receiveitemcost" type="text" value="1"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Price:&nbsp;</label></td>
							<td><input id="receiveitemprice" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Margin:&nbsp;</label></td>
							<td><input id="receiveitemmargin" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Total:&nbsp;</label></td>
							<td><input id="receiveitemtotal" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Tax:&nbsp;</label></td>
							<td><select id="receiveitemtax" class="taxselect" style="width: 100% !important;">
							</select></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Date:&nbsp;</label></td>
							<td><input id="receiveitemdate" type="text" readonly /></td>
						</tr>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="addreceivedialog" class="hide">
		<div class="row form-group">
			<div class="col-sm-6"><label>Supplier:</label><select id="newreceiveitemsupplier" class="supselect" required="true"></select></div>
			<div class="col-sm-6"><label>Invoice Number:</label><input id="newreceiveitemnumber" type="text" class="input-width" required="true"/></div>
		</div>
		<div class="row form-group">
			<div class="col-sm-6"><label>Invoice Total:</label><input id="newreceiveitemtotal" type="text" class="input-width"  value="" required="true" /><br/></div>
			<div class="col-sm-6"><label>Total:</label><input id="newreceiveitemstotal" type="text" class="input-width" required="true" readonly="true" /><br/></div>
		</div>

		<div class="row form-group">
			<div class="col-sm-6"><label>Product:</label><input id="newreceiveitemproduct" type="text" placeholder="Type product stock code and press enter" class="input-width-in"/></div>
			<div class="col-sm-6"><label>Date:</label><input id="newreceiveitemdate" type="text" class="input-width" required="true" /><br/></div>
			<!--<div class="col-sm-2"><button style="float: right; margin-right: 8px;" class="btn btn-primary btn-xs" onclick="addItemModifier({});">Add</button></div>-->
		</div>
		<hr>
		<div class="from-group">
			<table class="table table-stripped table-responsive" id="itemtabledetail" style="margin-bottom: 0; padding-left: 10px; margin-right: 10px;">
				<thead class="table-header smaller">
					<tr>
						<th><small>Stock Code</small></th>
						<th><small>Name</small></th>
						<th><small>Category</small></th>
						<th><small>Qty</small></th>
						<th><small>Tax</small></th>
						<th><small>Cost</small></th>
						<th><small>Price</small></th>
						<th><small>Margin</small></th>
						<th><small>Total</small></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="itemmodtable">

				</tbody>
			</table>
			 <!--<button style="float: right; margin-right: 8px;" class="btn btn-primary btn-xs" onclick="addItemModifier();">Add</button>-->
		</div>
	</div>

	<div id="stockreceivedialog" class="hide">
		<div style="width: 100%; overflow-x: auto;">
			<table id="detailInvoiceData" class="table table-responsive table-stripped">
				<thead>
					<tr>
						<th data-priority="0" class="center">
						<label>
							<input type="checkbox" class="ace" />
							<span class="lbl"></span>
						</label>
						</th>
						<th data-priority="1">ID</th>
						<th data-priority="3">Invoice No.</th>
						<th data-priority="2">Name</th>
					  <!--  <th data-priority="7">Tax</th>-->
						<th data-priority="4">Qty</th>
						<th data-priority="5">Cost</th>
						<th data-priority="6">Price</th>
						<th data-priority="7">Margin</th>
					<!--	<th data-priority="8">Invoice Total</th>-->
						<th data-priority="11">Total</th>
						<th data-priority="9">Category</th>
						<th data-priority="10">Supplier</th>
						<th data-priority="2">Action</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
	<!-- page specific plugin scripts -->
	<link rel="stylesheet" href="/admin/assets/js/csv-import/lib/jquery.ezdz.min.css"/>
	<!--<script type="text/javascript" src="/admin/assets/js/csv-import/lib/jquery.ezdz.min.js"></script>
	<script type="text/javascript" src="/admin/assets/js/csv-import/lib/jquery-sortable-min.js"></script>
	<script type="text/javascript" src="/admin/assets/js/csv-import/lib/jquery.csv-0.71.min.js"></script>
	<script type="text/javascript" src="/admin/assets/js/csv-import/csv.import.tool.js"></script>-->

	<!-- inline scripts related to this page -->
	<script type="text/javascript">
	    var receivestock = null;
	    var stockhist = null;
	    var suppliers = null;
	    var categories = null;
	    var datatable;
	    $(function() {
	        var data1 = WPOS.sendJsonData("receive/get", JSON.stringify({"receive/get":""}));
	        var data2 = WPOS.sendJsonData("suppliers/get", JSON.stringify({"suppliers/get":""}));
	        var data3 = WPOS.sendJsonData("categories/get", JSON.stringify({"categories/get":""}));

			receivestock = data1;
	        suppliers = data2;
	        categories = data3;

	        var receiveitemarray = [];
	        var tempitemreceive;
	        var taxrules = WPOS.getTaxTable().rules;
	        for (var key in receivestock){
	            tempitemreceive = receivestock[key];
	            if (taxrules.hasOwnProperty(tempitemreceive.taxid)){
	                tempitemreceive.taxname = taxrules[tempitemreceive.taxid].name;
	            } else {
	                tempitemreceive.taxname = "Not Defined";
	            }
	            receiveitemarray.push(tempitemreceive);
	        }

	        datatable = $('#receiveitemstable').dataTable({
	            "bProcessing": true,
	            "aaData": receiveitemarray,
	            "aaSorting": [[ 11, "desc" ]],
	            "aLengthMenu": [ 10, 25, 50, 100, 200],
	            "aoColumns": [
	                { "mData":null, sDefaultContent:'<div style="text-align: center"><label><input class="ace dt-select-cb" type="checkbox"><span class="lbl"></span></label><div>', bSortable: false },
	                { "mData":"id" },
	                { "mData":"invoice_number" },
	                { "mData":"invoice_total" },
	                { "mData":"name" },
	               // { "mData":"qty" },
	               // { "mData":"cost" },
	                { "mData":function(data,type,val){return (data['cost']==""?"":WPOS.util.currencyFormat(data["cost"]));} },
	                { "mData":function(data,type,val){return (data['price']==""?"":WPOS.util.currencyFormat(data["price"]));} },
	                { "mData":"margin" },
	               { "mData":function(data,type,val){return (data['total_sum']==""?"":WPOS.util.currencyFormat(data["total_sum"]));} },
	                { "mData":function(data,type,val){return (categories.hasOwnProperty(data.categoryid)?categories[data.categoryid].name:'None'); } },
	                { "mData":function(data,type,val){return (suppliers.hasOwnProperty(data.supplierid)?suppliers[data.supplierid].name:'None'); } },
	                { "mData":"created" },
	                //{ mData:function(data,type,val){return '<div class="action-buttons"><a class="red" onclick="getReceiveItemsOfNumber('+data.invoice_number+');"><i class="icon-time bigger-130"></i></a></div>'; }, "bSortable": false },
	                { "mData":null, sDefaultContent:'<div class="action-buttons"><a class="green" onclick="openEditDialog($(this).closest(\'tr\').find(\'td\').eq(1).text());"><i class="icon-pencil bigger-130"></i></a><a class="red" onclick="removeItem($(this).closest(\'tr\').find(\'td\').eq(1).text())"><i class="icon-trash bigger-130"></i></a></div>', "bSortable": false }
	            ],
	            "columns": [
	                {},
	                {type: "numeric"},
	                {type:  "string"},
	                {type: "numeric"},
	                {type: "numeric"},
	                {type:  "string"},
	                {type: "numeric"},
	                {type: "numeric"},
	                {type:  "string"},
	                {type: "numeric"},
	                {type:  "string"},
	                {type:  "string"},
	                {}
	            ],
	            "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	                // Add selected row count to footer
	                var selected = this.api().rows('.selected').count();
	                return sPre+(selected>0 ? '<br/>'+selected+' row(s) selected <span class="action-buttons"><a class="red" onclick="removeSelectedItems();"><i class="icon-trash bigger-130"></i></a></span>':'');
	            }
	        });

	        // row selection checkboxes
	        datatable.find("tbody").on('click', '.dt-select-cb', function(e){
	            var row = $(this).parents().eq(3);
	            if (row.hasClass('selected')) {
	                row.removeClass('selected');
	            } else {
	                row.addClass('selected');
	            }
	            datatable.api().draw(false);
	            e.stopPropagation();
	        });

	        $('table.dataTable th input:checkbox').on('change' , function(){
	            var that = this;
	            $(this).closest('table.dataTable').find('tr > td:first-child input:checkbox')
	                .each(function(){
	                    var row = $(this).parents().eq(3);
	                    if ($(that).is(":checked")) {
	                        row.addClass('selected');
	                        $(this).prop('checked', true);
	                    } else {
	                        row.removeClass('selected');
	                        $(this).prop('checked', false);
	                    }
	                });
	            datatable.api().draw(false);
	        });

	        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
	        function tooltip_placement(context, source) {
	            var $source = $(source);
	            var $parent = $source.closest('table');
	            var off1 = $parent.offset();
	            var w1 = $parent.width();

	            var off2 = $source.offset();
	            var w2 = $source.width();

	            if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
	            return 'left';
	        }
	        // dialogs
	        $( "#addreceivedialog" ).removeClass('hide').dialog({
	                resizable: false,
	                width: 'auto',
	                modal: true,
	                autoOpen: false,
	                title: "Receive Items ",
	                title_html: true,
	                buttons: [
	                    {
	                        html: "<i class='icon-save bigger-110'></i>&nbsp; Save",
	                        "class" : "btn btn-success btn-xs",
	                        click: function() {

								if($("#itemtabledetail tr").length > 1){
										saveItem(true);
								}
								else{
									alert("Please add items first");
								}
	                        }
	                    }
	                    ,
	                    {
	                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
	                        "class" : "btn btn-xs",
	                        click: function() {
								 if (confirm("Are you sure you want to cancel adding items?") == true) {
										$("#addreceivedialog")
										.find("input,select")
										.val('')
										.end();
										 var inc = $("#itemmodtable tr").length;
										 for(var i = 0; i <= inc; i++){
											removeItemInAddModal(i);
										 }
										 $( this ).dialog( "close" );
										 $("#newreceiveitemstotal").val('');

									} else {

									}
	                        }
	                    }
	                ],
	                create: function( event, ui ) {
	                    // Set maxWidth
	                    $(this).css("maxWidth", "460px");
	                }
	        });
	        $( "#editreceivedialog" ).removeClass('hide').addClass('editreceiveitems').dialog({
					resizable: false,
					width: 'auto',
					modal: true,
					autoOpen: false,
					title: "Edit Item",
					title_html: true,
					buttons: [
						{
							html: "<i class='icon-save bigger-110'></i>&nbsp; Update",
							"class" : "btn btn-success btn-xs",
							click: function() {
								saveItem(false);
							}
						}
						,
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
							"class" : "btn btn-xs",
							click: function() {
								$( this ).dialog( "close" );
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "460px");
					}
	        });

	         $( "#stockreceivedialog" ).removeClass('hide').dialog({
					resizable: false,
					width: 'auto',
					maxWidth: '700px',
					modal: true,
					autoOpen: false,
					title: "Received Items",
					title_html: true,
					buttons: [
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Close",
							"class" : "btn btn-xs",
							 click: function() {
								$( this ).dialog( "close" );
								  reloadGroupedData();
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "700px");
					}
        });
	         // populate category & supplier records in select boxes
						var supsel = $(".supselect");
						supsel.html('');
						supsel.append('<option class="supid-0" value="">None</option>');
						for (key in suppliers){
							supsel.append('<option class="supid-'+suppliers[key].id+'" value="'+suppliers[key].id+'">'+suppliers[key].name+'</option>');
						}

					    var catsel = $(".catselect");
						catsel.html('');
						catsel.append('<option class="catid-0" value="0">None</option>');
						for (key in categories){
							catsel.append('<option class="catid-'+categories[key].id+'" value="'+categories[key].id+'">'+categories[key].name+'</option>');
						}


					// populate tax records in select boxes
					var taxsel = $(".taxselect");
					taxsel.html('');
					for (key in WPOS.getTaxTable().rules){
						taxsel.append('<option class="taxid-'+WPOS.getTaxTable().rules[key].id+'" value="'+WPOS.getTaxTable().rules[key].id+'">'+WPOS.getTaxTable().rules[key].name+'</option>');
					}


	        $("#newreceiveitemproduct").keyup(function(e){
				if(e.keyCode === 13){
						var find =  $("#newreceiveitemproduct").val();
						if(find == "")
						return true;
						var inc = $("#itemmodtable tr").length;
						for(var i=0; i <= inc; i++){
							var existcode = $("#newreceiveitemcode"+i+"").val();
							if(existcode == find.toUpperCase()){
								alert("The item is already added");
								return true;
							}
						}
						  result = WPOS.sendJsonData("receive/find", JSON.stringify(find));
						  if(result != false){
							addItemModifier(result);
							//alert(JSON.stringify(result));
						}
						else{
							addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[]}});
						}
				}
				});

				$("#receiveitemprice, #receiveitemcost, #newreceiveitemqty").keyup(function(){
				   calculateMarginTotalEDIT();
		    	});
		    	 $("#receiveitemqty").mouseup(function(){
				   calculateMarginTotalEDIT();
		    	});
		    		  var now = new Date();
			          var month = now.getMonth()+1;
			          if(month<10) {
							month='0'+month
					  }
					  var day = now.getDate();
					  if(day<10) {
							day='0'+day
					  }
			          var date = month+'/'+day+'/'+now.getFullYear();
			          $("#newreceiveitemdate").val(date);

		    	      $("#newreceiveitemdate").datepicker();

			 // hide loader
	        WPOS.util.hideLoader();
	    });
	      function getReceiveItemsOfNumber(number){
			WPOS.util.showLoader();
			 $(function() {
			 stockhist = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: number}));
			// populate stock dialog with list

				//for (var i in stockhist){
				//	hist = stockhist[i];
				//	$("#stockhisttable").append('<tr><td>'+hist.id+'</td><td>'+hist.invoice_number+'</td><td>'+hist.name+'</td><td>'+hist.qty+'</td><td>'+hist.cost+'</td><td>'+hist.price+'</td><td>'+hist.margin+'</td><td>'+hist.total+'</td><td>'+hist.categoryid+'</td><td>'+hist.supplierid+'</td><td><div class="action-buttons"><a class="green" onclick="openEditDialog($(this).closest(\'tr\').find(\'td\').eq(1).text());"><i class="icon-pencil bigger-130"></i></a><a class="red" onclick="removeItem('+hist.id+')"><i class="icon-trash bigger-130"></i></a></div></td></tr>');
				//}
			//==================================================================================//
					var receiveitemarray1 = [];
					var tempitemreceive1;
					var taxrules = WPOS.getTaxTable().rules;
					for (var key in stockhist){
						tempitemreceive1 = stockhist[key];
						if (taxrules.hasOwnProperty(tempitemreceive1.taxid)){
							tempitemreceive1.taxname = taxrules[tempitemreceive1.taxid].name;
						} else {
							tempitemreceive1.taxname = "Not Defined";
						}
						receiveitemarray1.push(tempitemreceive1);
					}

					datatables = $('#detailInvoiceData').dataTable({
						"destroy": true,
						"bProcessing": true,
						"aaData": receiveitemarray1,
						"aaSorting": [[ 1, "desc" ]],
						"aLengthMenu": [ 10, 25, 50, 100, 200],
						"aoColumns": [
							{ "mData":null, sDefaultContent:'<div style="text-align: center"><label><input class="ace dt-select-cb" type="checkbox"><span class="lbl"></span></label><div>', bSortable: false },
							{ "mData":"id" },
							{ "mData":"invoice_number" },
							{ "mData":"name" },
							{ "mData":"qty" },
							{ "mData":"cost" },
							{ "mData":function(data,type,val){return (data['price']==""?"":WPOS.util.currencyFormat(data["price"]));} },
							{ "mData":"margin" },
							//{ "mData":"invoice_total" },
							{ "mData":"total" },
							{ "mData":function(data,type,val){return (categories.hasOwnProperty(data.categoryid)?categories[data.categoryid].name:'None'); } },
							{ "mData":function(data,type,val){return (suppliers.hasOwnProperty(data.supplierid)?suppliers[data.supplierid].name:'None'); } },
							{ mData:function(data,type,val){return '<div class="action-buttons"><a class="green" onclick="openEditDialog($(this).closest(\'tr\').find(\'td\').eq(1).text());"><i class="icon-pencil bigger-130"></i></a><a class="red" onclick="removeItem('+data.id+','+data.invoice_number+')"><i class="icon-trash bigger-130"></i></a></div>'; }, "bSortable": false },
						   // { "mData":null, sDefaultContent:'<div class="action-buttons"><a class="green" onclick="openEditDialog($(this).closest(\'tr\').find(\'td\').eq(1).text());"><i class="icon-pencil bigger-130"></i></a><a class="red" onclick="removeItem($(this).closest(\'tr\').find(\'td\').eq(1).text())"><i class="icon-trash bigger-130"></i></a></div>', "bSortable": false }
						],
						"columns": [
							{},
							{type: "numeric"},
							{type: "string"},
							{type: "string"},
							{type: "string"},
							{type: "numeric"},
							{type: "currency"},
							{type: "string"},
							{type: "numeric"},
							{type: "numeric"},
							{type: "string"},
							{type: "string"},
							{}
						],
						"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
							// Add selected row count to footer
							var selected = this.api().rows('.selected').count();
							return sPre+(selected>0 ? '<br/>'+selected+' row(s) selected <span class="action-buttons"><a class="red" onclick="removeSelectedItems();"><i class="icon-trash bigger-130"></i></a></span>':'');
						}
					});
					  // row selection checkboxes
							datatables.find("tbody").on('click', '.dt-select-cb', function(e){
								var row = $(this).parents().eq(3);
								if (row.hasClass('selected')) {
									row.removeClass('selected');
								} else {
									row.addClass('selected');
								}
								datatables.api().draw(false);
								e.stopPropagation();
							});

							$('table.dataTable th input:checkbox').on('change' , function(){
								var that = this;
								$(this).closest('table.dataTable').find('tr > td:first-child input:checkbox')
									.each(function(){
										var row = $(this).parents().eq(3);
										if ($(that).is(":checked")) {
											row.addClass('selected');
											$(this).prop('checked', true);
										} else {
											row.removeClass('selected');
											$(this).prop('checked', false);
										}
									});
								datatables.api().draw(false);
							});

							$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
							function tooltip_placement(context, source) {
								var $source = $(source);
								var $parent = $source.closest('table');
								var off1 = $parent.offset();
								var w1 = $parent.width();

								var off2 = $source.offset();
								var w2 = $source.width();

								if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
								return 'left';
							}

			//=================================================================================//
			});
			WPOS.util.hideLoader();
			$("#stockreceivedialog").dialog('open');
		}
	    // updating records
	    function openEditDialog(id){
	        var item = receivestock[id];

	          $("#receiveitemid").val(item.id);
	          $("#receiveitemsupplier").val(item.supplierid);
	          $("#receiveitemcategory").val(item.categoryid);
	          $("#receiveitemnumber").val(item.invoice_number);
	          $("#receiveiteminvoicetotal").val(item.invoice_total);
	          //item.product = $("#newreceiveitemproduct").val();
	          $("#receiveitemcode").val(item.code);
	          $("#receiveitemname").val(item.name);
	          $("#receiveitemqty").val(item.qty);
	          $("#receiveitemcost").val(item.cost);
	          $("#receiveitemprice").val(item.price);
	          $("#receiveitemmargin").val(item.margin);
	          $("#receiveitemtotal").val(item.total);
	          $("#receiveitemdate").val(item.created);

	           // item.description = $("#newreceiveitemdesc").val();
	           // item.categoryid = $("#newreceiveitemcategory").val();
	           // item.type = "general";
	            //item.modifiers = [];

	       /* var modtable = $("#itemmodtable");
	        var modselecttable = $("#itemselmodtable");
	        modtable.html('');
	        modselecttable.html('');
	        if (item.hasOwnProperty('modifiers')){
	            var mod;
	            for (var i=0; i<item.modifiers.length; i++){
	                mod = item.modifiers[i];
	                if (mod.type=='select'){
	                    var modopttable = '';
	                    for (var o=0; o<mod.options.length; o++){
	                        modopttable += '<tr><td><input onclick="handleSelectCheckbox(this);" type="checkbox" class="modoptdefault" '+(mod.options[o].default==true?'checked="checked"':'')+'/></td><td><input style="width: 130px" type="text" class="modoptname" value="'+mod.options[o].name+'"/></td><td><input type="text" style="width: 60px" class="modoptprice" value="'+mod.options[o].price+'"/></td><td style="text-align: right;"><button class="btn btn-danger btn-xs" onclick="$(this).parent().parent().remove();">X</button></td></tr>';
	                    }
	                    modselecttable.append('<tr class="selmoditem"><td colspan="4" style="padding-right: 0; padding-left: 0;"><div style="padding-left: 8px; padding-right: 8px;"><label>Name:</label>&nbsp;<input style="width: 130px" type="text" class="modname" value="'+mod.name+'"/><button class="btn btn-danger btn-xs pull-right" style="margin-left: 5px;" onclick="$(this).parents().eq(2).remove();">X</button></div><table class="table" style="margin-top: 5px;">'+modtableheader+'<tbody class="modoptions">'+modopttable+'</tbody></table></td></tr>');
	                } else {
	                    modtable.append('<tr><td><input type="text" style="width: 40px" class="modqty" value="'+mod.qty+'"/></td><td><input type="text" style="width: 40px" class="modminqty" value="'+mod.minqty+'"/></td><td><input type="text" style="width: 40px" class="modmaxqty" value="'+mod.maxqty+'"/></td><td><input style="width: 130px" type="text" class="modname" value="'+mod.name+'"/></td><td><input type="text" style="width: 60px" class="modprice" value="'+mod.price+'"/></td><td style="text-align: right;"><button class="btn btn-danger btn-xs" onclick="$(this).parent().parent().remove();">X</button></td></tr>');
	                }
	            }
	        }*/
	        $("#editreceivedialog").dialog("open");

	    }

	    var num = 1;
	    function addItemModifier(itemData){
			var inc = $("#itemmodtable tr").length;
			var cat,tax;
			var totrows = inc+1;

			if(JSON.stringify(itemData) === JSON.stringify({})){
				//$("#itemmodtable").append('<tr class="item"><td>'+totrows+'<input type="hidden" id="itemstatus'+inc+'"  value="1"></td><td><input style="width : 90px" type="text" class="modname itemval" name="newreceiveitemcode"  id="newreceiveitemcode'+inc+'" value=""/></td><td><input style="width: 110px" type="text" class="modname itemval" name="newreceiveitemname"  id="newreceiveitemname'+inc+'" value=""/></td><td><select id="newreceiveitemcategory'+inc+'" class="catselect"></select></td><td><input type="number" style="width: 60px" class="modminqty itemval" min="1"  name="newreceiveitemqty" id="newreceiveitemqty'+inc+'" value="1"/></td><td><select id="newreceiveitemtax'+inc+'" class="taxselect"></select></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="newreceiveitemcost" id="newreceiveitemcost'+inc+'" value="0.00"/></td><td><input type="text" style="width: 70px" name="newreceiveitemprice" id="newreceiveitemprice'+inc+'" class="modprice itemval" value="0.00"/></td><td><input type="text" style="width: 70px" name="newreceiveitemmargin" id="newreceiveitemmargin'+inc+'" class="modminqty itemval" value="0"/></td><td><input type="text" style="width: 50px" name="newreceiveitemsum" id="newreceiveitemsum'+inc+'" class="modminqty itemval" value="0"/></td><td style="text-align: right;"><button id="removeadditem'+inc+'" class="btn btn-danger btn-xs" onclick="removeItemInAddModal('+inc+')">X</button></td></tr>'
	              $("#itemmodtable").append('<tr class="item"><td><input style="width : 90px" type="text" class="modname itemval" name="newreceiveitemcode"  id="newreceiveitemcode'+inc+'" value=""/><input type="hidden" id="itemstatus'+inc+'"  value="1"></td><td><input style="width: 110px" type="text" class="modname itemval" name="newreceiveitemname"  id="newreceiveitemname'+inc+'" value=""/></td><td><select id="newreceiveitemcategory'+inc+'" class="catselect"></select></td><td><input type="number" style="width: 60px" class="modminqty itemval" min="1"  name="newreceiveitemqty" id="newreceiveitemqty'+inc+'" value="1"/></td><td><select id="newreceiveitemtax'+inc+'" class="taxselect"></select></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="newreceiveitemcost" id="newreceiveitemcost'+inc+'" value="0.00"/></td><td><input type="text" style="width: 70px" name="newreceiveitemprice" id="newreceiveitemprice'+inc+'" class="modprice itemval" value="0.00"/></td><td><input type="text" style="width: 70px" name="newreceiveitemmargin" id="newreceiveitemmargin'+inc+'" class="modminqty itemval" value="0"/></td><td><input type="text" style="width: 50px" name="newreceiveitemsum" id="newreceiveitemsum'+inc+'" class="modminqty itemval" value="0" readonly/></td><td style="text-align: right;"><button id="removeadditem'+inc+'" class="btn btn-danger btn-xs" onclick="removeItemInAddModal('+inc+')">X</button></td></tr>');
			} else {
				 for (var i in itemData){

					 //$("#itemmodtable").append('<tr class="item"><td> '+totrows+'<input id="itemstatus'+inc+'" type="hidden" value="0"></td><td><input style="width : 90px" type="text" class="modname itemval" name="newreceiveitemcode"  id="newreceiveitemcode'+inc+'" value="'+itemData[i].code+'" readonly="true"/></td><td><input style="width: 110px" type="text" class="modname itemval" name="newreceiveitemname"  id="newreceiveitemname'+inc+'" value="'+itemData[i].name+'"/></td><td><select id="newreceiveitemcategory'+inc+'" class="catselect"></select></td><td><input type="number" style="width: 60px" class="modminqty itemval" min="1"  name="newreceiveitemqty" id="newreceiveitemqty'+inc+'" value="'+itemData[i].qty+'"/></td><td><select id="newreceiveitemtax'+inc+'" class="taxselect"></select></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="newreceiveitemcost" id="newreceiveitemcost'+inc+'" value="'+itemData[i].cost+'"/></td><td><input type="text" style="width: 70px" name="newreceiveitemprice" id="newreceiveitemprice'+inc+'" class="modprice itemval" value="'+itemData[i].price+'"/></td><td><input type="text" style="width: 70px" name="newreceiveitemmargin" id="newreceiveitemmargin'+inc+'" class="modminqty itemval" value="0"/></td><td><input type="text" style="width: 50px" name="newreceiveitemsum" id="newreceiveitemsum'+inc+'" class="modminqty itemval" value="'+itemData[i].total+'"/></td><td style="text-align: right;"><button id="removeadditem'+inc+'" class="btn btn-danger btn-xs" onclick="removeItemInAddModal('+inc+')">X</button></td></tr>');
					 $("#itemmodtable").append('<tr class="item"><td><input style="width : 90px" type="text" class="modname itemval" name="newreceiveitemcode"  id="newreceiveitemcode'+inc+'" value="'+itemData[i].code+'" readonly="true"/><input id="itemstatus'+inc+'" type="hidden" value="0"></td><td><input style="width: 110px" type="text" class="modname itemval" name="newreceiveitemname"  id="newreceiveitemname'+inc+'" value="'+itemData[i].name+'"/></td><td><select id="newreceiveitemcategory'+inc+'" class="catselect"></select></td><td><input type="number" style="width: 60px" class="modminqty itemval" min="1"  name="newreceiveitemqty" id="newreceiveitemqty'+inc+'" value="'+itemData[i].qty+'"/></td><td><select id="newreceiveitemtax'+inc+'" class="taxselect"></select></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="newreceiveitemcost" id="newreceiveitemcost'+inc+'" value="'+itemData[i].cost+'"/></td><td><input type="text" style="width: 70px" name="newreceiveitemprice" id="newreceiveitemprice'+inc+'" class="modprice itemval" value="'+itemData[i].price+'"/></td><td><input type="text" style="width: 70px" name="newreceiveitemmargin" id="newreceiveitemmargin'+inc+'" class="modminqty itemval" value="0"/></td><td><input type="text" style="width: 50px" name="newreceiveitemsum" id="newreceiveitemsum'+inc+'" class="modminqty itemval" value="'+itemData[i].total+'" readonly/></td><td style="text-align: right;"><button id="removeadditem'+inc+'" class="btn btn-danger btn-xs" onclick="removeItemInAddModal('+inc+')">X</button></td></tr>');
			          tax = itemData[i].taxid; cat = itemData[i].categoryid;

			       }
			       calculationMarginTotal(inc);
			       $("#newreceiveitemproduct").val("");

			 }
	         $(function(){
				   //$("#newreceiveitemcategory"+inc+"");
	               var catsel = $("#newreceiveitemcategory"+inc);
					catsel.html('');
					catsel.append('<option class="catid-0" value="0">None</option>');
					for (key in categories){
							catsel.append('<option class="catid-'+categories[key].id+'" value="'+categories[key].id+'">'+categories[key].name+'</option>');
					}
					if(cat)
					$("#newreceiveitemcategory"+inc).val(cat);


					// populate tax records in select boxes
					var taxsel = $("#newreceiveitemtax"+inc);
					taxsel.html('');
					for (key in WPOS.getTaxTable().rules){
						taxsel.append('<option class="taxid-'+WPOS.getTaxTable().rules[key].id+'" value="'+WPOS.getTaxTable().rules[key].id+'">'+WPOS.getTaxTable().rules[key].name+'</option>');
					}
					if(tax)
					$("#newreceiveitemtax"+inc).val(tax);


				     // $("#newreceiveitemdate").datepicker();

	           });

	           $("#newreceiveitemprice"+inc+", #newreceiveitemcost"+inc+", #newreceiveitemqty"+inc).keyup(function(){
				   calculationMarginTotal(inc);
		    	});
		    	 $("#newreceiveitemqty"+inc).mouseup(function(){
				   calculationMarginTotal(inc);
		    	});

			num = num + 1;
	    }

	 /*   function addSelectItemModifier(){
	        var modseltable = $("#itemselmodtable");
	        var modelem = $('<tr class="selmoditem"><td colspan="4" style="padding-right: 0; padding-left: 0;"><div style="padding-left: 8px; padding-right: 8px;"><label>Name:</label>&nbsp;<input style="width: 130px" type="text" class="modname" value=""/><button class="btn btn-danger btn-xs pull-right" style="margin-left: 5px;" onclick="$(this).parents().eq(2).remove();">X</button></div><table class="table" style="margin-top: 5px;">'+modtableheader+'<tbody class="modoptions">'+modselectoption+'</tbody></table></td></tr>');
				modelem.find('.modoptdefault').prop('checked', true);
				modseltable.append(modelem);
	    }*/

	    var modtableheader = '<thead class="table-header smaller"><tr><th><small>Default</small></th><th><small>Name</small></th><th><small>Price</small></th><th><button class="btn btn-primary btn-xs pull-right" onclick="addSelectModItem($(this).parents().eq(3).find(\'.modoptions\'));">Add Option</button></th></tr></thead>';
	    var modselectoption = '<tr><td><input onclick="handleSelectCheckbox($(this));" type="checkbox" class="modoptdefault"/></td><td><input style="width: 130px" type="text" class="modoptname" value=""/></td><td><input type="text" style="width: 60px" class="modoptprice" value="0.00"/></td><td style="text-align: right;"><button class="btn btn-danger btn-xs" onclick="$(this).parents().eq(1).remove();">X</button></td></tr>';

	    function addSelectModItem(elem){
	        $(elem).append(modselectoption);
				if (elem.find('tr').length==1) $(elem).find('.modoptdefault').prop('checked', true);
	    }

	    function handleSelectCheckbox(elem){
	        var table = $(elem).parent().parent().parent();
				table.find('.modoptdefault').prop('checked', false);
				$(elem).prop('checked', true);
	    }

	    function saveItem(isnewitem){
	        // show loader
	        WPOS.util.showLoader();
	        var item = {
				product : []
				 };
	       var result,costval;

	        if (isnewitem){
				var rows = $("#itemmodtable tr").length;
				var i=0;
	            // adding a new item

	            item.supplierid = $("#newreceiveitemsupplier").val();
	            item.invoice_number = $("#newreceiveitemnumber").val();
	            item.invoice_total = $("#newreceiveitemtotal").val();
	            item.totals_sum = $("#newreceiveitemstotal").val();
	            item.created = $("#newreceiveitemdate").val();

	           for(i = 0; i < rows; i++){
				   var test = {}
				    test.status = $("#itemstatus"+i).val();
				    test.code = $("#newreceiveitemcode"+i).val();
					test.name = $("#newreceiveitemname"+i).val();
					test.categoryid = $("#newreceiveitemcategory"+i).val();
					test.qty = $("#newreceiveitemqty"+i).val();
					test.taxid = $("#newreceiveitemtax"+i).val();
					costval = $("#newreceiveitemcost"+i).val();
					test.cost = (costval ? costval : 0);
					test.price = $("#newreceiveitemprice"+i).val();
					test.margin = $("#newreceiveitemmargin"+i).val();
					test.total = $("#newreceiveitemsum"+i).val();
					item['product'].push(test);
			   }

	            result = WPOS.sendJsonData("receive/add", JSON.stringify(item));

	            if (result!==false){
	                receivestock[result.id] = result;
	                reloadGroupedData();
	                // reloadTable();
	                $("#addreceivedialog").dialog("close");
	                $("#addreceivedialog")
					.find("input,select")
					.val('')
					.end();
					 var inc = $("#itemmodtable tr").length;
					  for(var i = 0; i <= inc; i++){
						removeItemInAddModal(i);
					 }
					  $("#newreceiveitemstotal").val('');
	            }
	        } else {

	            // updating an item
	            item.id = $("#receiveitemid").val();
	            item.supplierid = $("#receiveitemsupplier").val();
	            item.categoryid = $("#receiveitemcategory").val();
	            item.invoice_number = $("#receiveitemnumber").val();
	            item.invoice_total = $("#receiveiteminvoicetotal").val();
	            item.code = $("#receiveitemcode").val();
	            item.name = $("#receiveitemname").val();
	            item.qty = $("#receiveitemqty").val();
	            costval = $("#receiveitemcost").val();
	            item.cost = (costval ? costval : 0);
	            item.price = $("#receiveitemprice").val();
	            item.margin = $("#receiveitemmargin").val();
	            item.taxid = $("#receiveitemtax").val();
	            item.total = $("#receiveitemtotal").val();
	            item.created = $("#receiveitemdate").val();

	           /* item.modifiers = [];
	            $("#itemselmodtable .selmoditem").each(function(){
	                var mod = {type:"select", options:[]};
	                mod.name = $(this).find(".modname").val();
	                $(this).find('.modoptions tr').each(function(){
	                    var modoption = {};
	                    modoption.default = $(this).find(".modoptdefault").is(':checked');
	                    modoption.name = $(this).find(".modoptname").val();
	                    modoption.price = $(this).find(".modoptprice").val();
	                    mod.options.push(modoption);
	                });
	                item.modifiers.push(mod);
	            });
	            $("#itemmodtable tr").each(function(){
	               var mod = {type:"simple"};
	               mod.qty = $(this).find(".modqty").val();
	               mod.minqty = $(this).find(".modminqty").val();
	               mod.maxqty = $(this).find(".modmaxqty").val();
	               mod.name = $(this).find(".modname").val();
	               mod.price = $(this).find(".modprice").val();
	               item.modifiers.push(mod);
	            });*/
	            result = WPOS.sendJsonData("receive/edit", JSON.stringify(item));
	            if (result!==false){
	                receivestock[result.id] = result;
	                reloadData(item.invoice_number);
	                $("#editreceivedialog").dialog("close");
	            }
	        }
	        // hide loader
	        WPOS.util.hideLoader();
	    }

	    function removeItemInAddModal(val){
			var sum = $("#newreceiveitemsum"+val+"").val();
			$("#removeadditem"+val+"").parent().parent().remove();
			var total = $("#newreceiveitemstotal").val();
			var newtotal = total-sum;
			 $("#newreceiveitemstotal").val(newtotal);
		}

		function calculationMarginTotal(inc){
			  var cost = $("#newreceiveitemcost"+inc).val();
			  var price = $("#newreceiveitemprice"+inc).val();
			  var qty = $("#newreceiveitemqty"+inc).val();
			  $("#newreceiveitemsum"+inc).val(Number(cost)*Number(qty));

				   if(cost != "" && price!= "" ){
					   margin = ((Number(price)-Number(cost))/Number(price)) * 100;
					   margin = margin.toFixed(2)
							if(margin == "-Infinity" || isNaN(margin)){
								$('#newreceiveitemmargin'+inc).val(0);
							}
							else{
								$('#newreceiveitemmargin'+inc).val(margin+"%");
							}
				   }else{
					   $("#newreceiveitemmargin"+inc).val(0);
				   }

			 var invoice_total = 0;
			 var rows = $("#itemmodtable tr").length;
			      for(var i = 0; i < rows; i++){
					var val=$("#newreceiveitemsum"+i).val();
					invoice_total = Number(invoice_total) + Number(val);
				}
				$("#newreceiveitemstotal").val(invoice_total);
		}

		function calculateMarginTotalEDIT(){
			  var cost = $("#receiveitemcost").val();
			  var price = $("#receiveitemprice").val();
			  var qty = $("#receiveitemqty").val();
			  $("#receiveitemtotal").val(Number(cost)*Number(qty));

				   if(cost != "" && price!= "" ){
					   margin = ((Number(price)-Number(cost))/Number(price)) * 100;
					   margin = margin.toFixed(2)
							if(margin == "-Infinity" || isNaN(margin)){
								$('#receiveitemmargin').val(0);
							}
							else{
								$('#receiveitemmargin').val(margin+"%");
							}
				   }else{
					   $("#receiveitemmargin").val(0);
				   }
		}

	    function removeItem(id, number){
		    var answer = confirm("Are you sure you want to delete this item?");
	        if (answer){
	            // show loader
	            WPOS.util.hideLoader();
	            if (WPOS.sendJsonData("receive/delete", '{"id":'+id+'}')){
	                delete receivestock[id];
	                reloadData(number);
	                //reloadTable();
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
	    }

	    function removeSelectedItems(){
	        var ids = datatable.api().rows('.selected').data().map(function(row){ return row.id });

	        var answer = confirm("Are you sure you want to delete "+ids.length+" selected items?");
	        if (answer){
	            // show loader
	            WPOS.util.hideLoader();
	            if (WPOS.sendJsonData("receive/delete", '{"id":"'+ids.join(",")+'"}')){
	                for (var i=0; i<ids.length; i++){
	                    delete receivestock[ids[i]];
	                }
	                reloadData();
	                //reloadTable();
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
	    }

	    function reloadData(number){
	        receivestock = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: number}));
	        reloadTable();
	    }

	    function reloadTable(){
	        var receiveitemarray1 = [];
	        var tempitemreceive1;
	        /*for (var key in receivestock){
	            tempitemreceive = receivestock[key];
	            tempitemreceive.taxname = WPOS.getTaxTable().rules[tempitemreceive.taxid].name;
	            receiveitemarray.push(tempitemreceive);
	        }*/

	         for (var key in receivestock){

	            tempitemreceive1 = receivestock[key];
	            receiveitemarray1.push(tempitemreceive1);
	        }
	        datatable.fnClearTable(false);
	        datatable.fnAddData(receiveitemarray1, false);
	        datatable.api().draw(false);
	    }

	     function reloadGroupedData(){
			   receivestock = WPOS.sendJsonData("receive/getGroupedData", JSON.stringify({"receive/getGroupedData":""}));
	        //receivestock = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: number}));
	        reloadGroupedDataTable();
	    }

	     function reloadGroupedDataTable(){
	        var receiveitemarray = [];
	        var tempitemreceive;
	        /*for (var key in receivestock){
	            tempitemreceive = receivestock[key];
	            tempitemreceive.taxname = WPOS.getTaxTable().rules[tempitemreceive.taxid].name;
	            receiveitemarray.push(tempitemreceive);
	        }*/

	         for (var key in receivestock){

	            tempitemreceive = receivestock[key];
	            receiveitemarray.push(tempitemreceive);
	        }
	        datatable.fnClearTable(false);
	        datatable.fnAddData(receiveitemarray, false);
	        datatable.api().draw(false);
	    }

		    var eventuiinit = false;

	    function initModalLoader(title){
	        $("#modalloader").removeClass('hide').dialog({
	            resizable: true,
	            width: 400,
	            modal: true,
	            autoOpen: false,
	            title: title,
	            title_html: true,
	            closeOnEscape: false,
	            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
	        });
	    }

	    function showModalLoader(title){
	        if (!eventuiinit){
	            initModalLoader(title);
	            eventuiinit = true;
	        }
	        $("#modalloader_status").text('Initializing...');
	        $("#modalloader_substatus").text('');
	        $("#modalloader_cbtn").hide();
	        $("#modalloader_img").show();
	        $("#modalloader_prog").show();
	        var modalloader = $("#modalloader");
	        modalloader.dialog('open');
	    }

	    function setModalLoaderProgress(progress){
	        $("#modalloader_progbar").attr('width', progress+"%")
	    }

	    function showModalCloseButton(result, substatus){
	        $("#modalloader_status").text(result);
	        setModalLoaderSubStatus(substatus? substatus : '');
	        $("#modalloader_img").hide();
	        $("#modalloader_prog").hide();
	        $("#modalloader_cbtn").show();
	    }

	    function setModalLoaderStatus(status){
	        $("#modalloader_status").text(status);
	    }

	    function setModalLoaderSubStatus(status){
	        $("#modalloader_substatus").text(status);
	    }
	</script>
	<div id="modalloader" class="hide" style="width: 360px; height: 320px; text-align: center;">
	    <img id="modalloader_img" style="width: 128px; height: auto;" src="/admin/assets/images/cloud_loader.gif"/>
	    <div id="modalloader_prog" class="progress progress-striped active">
	        <div class="progress-bar" id="modalloader_progbar" style="width: 100%;"></div>
	    </div>
	    <h4 id="modalloader_status">Initializing...</h4>
	    <h5 id="modalloader_substatus"></h5>
	    <button id="modalloader_cbtn" class="btn btn-primary" style="display: none; margin-top:40px;" onclick="$('#modalloader').dialog('close');">Close</button>
	</div>
	<style type="text/css">
	    #receiveitemstable_processing {
	        display: none;
	    }

	    body.dragging, body.dragging * {
	        cursor: move !important;
	    }

	    .dragged {
	        position: absolute;
	        opacity: 0.8;
	        z-index: 2000;
	    }

	    #dest_table li.excluded, #source_table li.excluded {
	        opacity: 0.8;
	        background-color: #f5f5f5;
	    }

	    .placeholder {
	        position: relative;
	        height: 40px;
	    }

	    .placeholder:before {
	        position: absolute;
	        /** Define arrowhead **/
	    }
	</style>
