	<style>

	.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons.ui-draggable, .ui-dialog-content.ui-widget-content{
	    max-width: 960px !important;
	    width: 100% !important;
	}
	#addreceivedialog label{width:120px;}
	#addreceivedialog .input-width{width:150px;}
	#addreceivedialog input{width:100%;}
	#addreceivedialog .supselect {
	    width: 150px;
	}
	.input-width-in {
	    width: 70% !important;
	}
	.item-detail-table{overflow:auto;max-height:200px;}
	</style>
	<!-- WallacePOS: Copyright (c) 2014 WallaceIT <micwallace@gmx.com> <https://www.gnu.org/licenses/lgpl.html> -->
	<div class="page-header">
	    <h1 style="margin-right: 20px; display: inline-block;">
	        Receive
	    </h1>
	    <button onclick="$('#itemtabledetail tr').remove();$('#itemmodtable').empty();$('#addreceivedialog').dialog('open');" id="addbtn" class="btn btn-primary btn-sm pull-right"><i class="icon-pencil align-top bigger-125"></i>Receive Items</button>
	</div><!-- /.page-header -->

	<div class="row">
	<div class="col-xs-12">
	<!-- PAGE CONTENT BEGINS -->

	<div class="row">
	<div class="col-xs-12">

	<div class="table-header">
	    Manage your business products
	</div>

	<table id="receiveitemstable" class="table table-striped table-bordered table-hover dt-responsive" style="width:100%;">
	<thead>
	<tr>
	    <th data-priority="3">Invoice Number</th>
	    <th data-priority="3">Total Items</th>
	    <th data-priority="2">Invoice Total</th>
	    <th data-priority="4">Total</th>
	    <th data-priority="4">Margin</th>
	    <th data-priority="5">Total Cost</th>
	    <th data-priority="6">Total Price</th>
	    <th data-priority="10">Supplier</th>
	    <th data-priority="11">Date</th>
	    <th class="noexport" data-priority="2"></th>
	</tr>
	</thead>
	<tbody>

	</tbody>
	</table>

	</div>
	</div>

	</div><!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
	<div id="editreceivedialog" class="hide">
	    <div class="tabbable" style="min-width: 360px; min-height: 310px;">
	        <ul class="nav nav-tabs">
	            <li class="active">
	                <a href="#receiveitemdetails" data-toggle="tab">
	                    Details
	                </a>
	            </li>
	        </ul>
	        <div class="tab-content" style="min-height: 320px;">
	            <div class="tab-pane active in" id="receiveitemdetails">
	                <table>
						<tr>
							<td style="text-align: right;"><label>Supplier:&nbsp;</label></td>
							<td><select id="receiveitemsupplier" class="supselect" style="width: 100% !important;">
								</select>
								<input id="receiveitemid" type="hidden"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Category:&nbsp;</label></td>
							<td><select id="receiveitemcategory" class="catselect" style="width: 100% !important;">
								</select>
								</td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Invoice Number:&nbsp;</label></td>
							<td><input id="receiveitemnumber" type="text" readonly="true"/><br/><input id="receiveitemid" type="hidden"/></td></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Invoice Total:&nbsp;</label></td>
							<td><input id="receiveiteminvoicetotal" type="text"/><br/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Stock Code:&nbsp;</label></td>
							<td><input id="receiveitemcode" type="text" value="0" readonly="true"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Name:&nbsp;</label></td>
							<td><input id="receiveitemname" type="text" value="0"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Qty:&nbsp;</label></td>
							<td><input id="receiveitemqty"  type="number" min="1" value="1"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Cost:&nbsp;</label></td>
							<td><input id="receiveitemcost" type="text" value="1"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Price:&nbsp;</label></td>
							<td><input id="receiveitemprice" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Margin:&nbsp;</label></td>
							<td><input id="receiveitemmargin" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Total:&nbsp;</label></td>
							<td><input id="receiveitemtotal" type="text"/></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Tax:&nbsp;</label></td>
							<td><select id="receiveitemtax" class="taxselect" style="width: 100% !important;">
							</select></td>
						</tr>
						<tr>
							<td style="text-align: right;"><label>Date:&nbsp;</label></td>
							<td><input id="receiveitemdate" type="text" readonly /></td>
						</tr>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="addreceivedialog" class="hide">
		<div class="row form-group">
			<div class="col-sm-6"><label>Supplier:</label><select id="newreceiveitemsupplier" style="width: 70% !important;" class="supselect" required="true"></select></div>
			<div class="col-sm-6"><label>Invoice Number:</label><input id="newreceiveitemnumber" type="text" class="input-width" required="true"/></div>
		</div>
		<div class="row form-group">
			<div class="col-sm-6"><label>Invoice Total:</label><input id="newreceiveitemtotal" type="text" class="input-width"  value="" required="true" /><br/></div>
			<div class="col-sm-6"><label>Total:</label><input id="newreceiveitemstotal" type="text" class="input-width" required="true" readonly="true" /><br/></div>
		</div>

		<div class="row form-group">
			<div class="col-sm-6"><label>Product:</label><div class="suggesstion-outer"><input id="newreceiveitemproduct" type="text" placeholder="Type product stock code and press enter"/><div id="suggesstion-box"><ul></ul></div></div></div>
			<div class="col-sm-6"><label>Date:</label><input id="newreceiveitemdate" type="text" class="input-width" required="true" /><br/></div>
		</div>
		<hr>
		<div class="from-group scroll-table-fixed-header">
			<div class="item-detail-table">
			<table class="table table-stripped table-responsive" id="itemtabledetail" style="margin-bottom: 0; padding-left: 10px; margin-right: 10px;">
				<thead class="table-header smaller">
					<tr>
						<th><small>Stock Code</small><div><small>Stock Code</small></div></th>
						<th><small>Name</small><div><small>Name</small></div></th>
						<th><small>Category</small><div><small>Category</small></div></th>
						<th><small>Qty</small><div><small>Qty</small></div></th>
						<th><small>Tax</small><div><small>Tax</small></div></th>
						<th><small>Cost</small><div><small>Cost</small></div></th>
						<th><small>Price</small><div><small>Price</small></div></th>
						<th><small>Margin</small><div><small>Margin</small></div></th>
						<th><small>Total</small><div><small>Total</small></div></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="itemmodtable">

				</tbody>
			</table>
			</div>
		</div>
	</div>
	<div id="additemtoreceivedialog" class="hide">
		<div class="row form-group">
			<div class="col-sm-6"><label class="dialog-class">Supplier:</label><select id="additemsupplier" style="width: 70% !important;" class="supselect" required="true"></select></div>
			<div class="col-sm-6"><label class="dialog-class">Invoice Number:</label><input id="additemmnumber" type="text" readonly class="input-width" required="true"/></div>
		</div>
		<div class="row form-group">
			<div class="col-sm-6"><label class="dialog-class">Invoice Total:</label><input id="additeminvoicetotal"  type="text" class="input-width"  value="" required="true" /><br/></div>
			<div class="col-sm-6"><label class="dialog-class">Total:</label><input id="additemstotal" type="text" class="input-width" required="true" readonly="true" /><br/></div>
		</div>

		<div class="row form-group">
			<div class="col-sm-6"><label class="dialog-class">Product:</label><div class="suggesstion-outer"><input id="additemproduct" type="text" placeholder="Type product stock code and press enter"/><div id="suggesstion-box-receive-items" class="forsuggesion" ><ul></ul></div></div></div>
			<div class="col-sm-6"><label class="dialog-class">Date:</label><input id="additemdate" readonly type="text" class="input-width" required="true" /><br/></div>
		</div>

		<hr>
		<div class="from-group scroll-table-fixed-header">
			<div class="forscroll">
				<table class="table table-stripped table-responsive" id="itemtabledetail" style="margin-bottom: 0; padding-left: 10px; margin-right: 10px;">
					<thead class="table-header smaller">
						<tr>
							<th><small>Stock Code</small><div><small>Stock Code</small></div></th>
							<th><small>Name</small><div><small>Name</small></div></th>
							<th><small>Category</small><div><small>Category</small></div></th>
							<th><small>Qty</small><div><small>Qty</small></div></th>
							<th><small>Tax</small><div><small>Tax</small></div></th>
							<th><small>Cost</small><div><small>Cost</small></div></th>
							<th><small>Price</small><div><small>Price</small></div></th>
							<th><small>Margin</small><div><small>Margin</small></div></th>
							<th><small>Total</small><div><small>Total</small></div></th>
							<th></th>
						</tr>
					</thead>
					<tbody id="additemmodtable">

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div id="stockreceivedialog" class="hide">
		<div style="width: 100%; overflow-x: auto;">
			<table id="detailInvoiceData" class="table table-responsive table-stripped">
				<thead>
					<tr>
						<th data-priority="0" class="center">
						<label>
							<input type="checkbox" class="ace" />
							<span class="lbl"></span>
						</label>
						</th>
						<th data-priority="1">ID</th>
						<th data-priority="3">Invoice No.</th>
						<th data-priority="2">Name</th>
						<th data-priority="4">Qty</th>
						<th data-priority="5">Cost</th>
						<th data-priority="6">Price</th>
						<th data-priority="7">Margin</th>
						<th data-priority="11">Total</th>
						<th data-priority="9">Category</th>
						<th data-priority="10">Supplier</th>
						<th data-priority="2">Action</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
	<!-- page specific plugin scripts -->
	<link rel="stylesheet" href="/admin/assets/js/csv-import/lib/jquery.ezdz.min.css"/>

	<!-- inline scripts related to this page -->
	<script type="text/javascript">
	    var receivestock = null;
	    var stockhist = null;
	    var suppliers = null;
	    var categories = null;
	    var datatable,datatables;
	    $(function() {
	        var data1 = WPOS.sendJsonData("receive/getGroupedData", JSON.stringify({"receive/getGroupedData":""}));
	        var data2 = WPOS.sendJsonData("suppliers/get", JSON.stringify({"suppliers/get":""}));
	        var data3 = WPOS.sendJsonData("categories/get", JSON.stringify({"categories/get":""}));

			receivestock = data1;
	        suppliers = data2;
	        categories = data3;

	        var receiveitemarray = [];
	        var tempitemreceive;
	        var taxrules = WPOS.getTaxTable().rules;
	        for (var key in receivestock){
	            tempitemreceive = receivestock[key];
	            if (taxrules.hasOwnProperty(tempitemreceive.taxid)){
	                tempitemreceive.taxname = taxrules[tempitemreceive.taxid].name;
	            } else {
	                tempitemreceive.taxname = "Not Defined";
	            }
	            receiveitemarray.push(tempitemreceive);
	        }

	        datatable = $('#receiveitemstable').dataTable({
	            "bProcessing": true,
	            "aaData": receiveitemarray,
	            "aaSorting": [[ 8, "desc" ]],
	            "aLengthMenu": [ 10, 25, 50, 100, 200],
	            "aoColumns": [
	                { "mData":"invoice_number" },
	                { "mData":"count" },
	                { "mData":"invoice_total" },
	                { "mData":"totals_sum" },
	                { "mData":"margin"},
	                { "mData":function(data,type,val){return (data['cost']==""?"":WPOS.util.currencyFormat(data["cost"]));} },
	                { "mData":function(data,type,val){return (data['price']==""?"":WPOS.util.currencyFormat(data["price"]));} },
	                { "mData":function(data,type,val){return (suppliers.hasOwnProperty(data.supplierid)?suppliers[data.supplierid].name:'None'); } },
	                { "mData":"created" },
	                { mData:function(data,type,val){ return '<div class="action-buttons"><a class="red" onclick=getReceiveItemsOfNumber("'+data.invoice_number+'","'+data.count+'","'+data.invoice_total+'","'+data.totals_sum+'","'+data.margin+'","'+data.cost+'","'+data.price+'",$(this).closest(\'tr\').find(\'td\').eq(7).text());><i class="icon-time bigger-130"></i></a><a class="" onclick=editCurrentReceive("'+data.invoice_number+'") ><i class="icon-pencil bigger-130"></i></a></div>'; }, "bSortable": false }
	            ],
	            "columns": [
	                {type: "string"},
	                {type: "numeric"},
	                {type: "numeric"},
	                {type: "numeric"},
	                {type: "numeric"},
	                {type: "string"},
	                {type: "numeric"},
	                {type: "string"},
	                {}
	            ],
	            "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	                // Add selected row count to footer
	                var selected = this.api().rows('.selected').count();
	                return sPre+(selected>0 ? '<br/>'+selected+' row(s) selected <span class="action-buttons"><a class="red" onclick="removeSelectedItems();"><i class="icon-trash bigger-130"></i></a></span>':'');
	            }
	        });
			//onclick=$("#additemtoreceivedialog").data("param_1","'+data.invoice_number+'").dialog("open");
	        // row selection checkboxes
	        datatable.find("tbody").on('click', '.dt-select-cb', function(e){
	            var row = $(this).parents().eq(3);
	            if (row.hasClass('selected')) {
	                row.removeClass('selected');
	            } else {
	                row.addClass('selected');
	            }
	            datatable.api().draw(false);
	            e.stopPropagation();
	        });

	        $('table.dataTable th input:checkbox').on('change' , function(){
	            var that = this;
	            $(this).closest('table.dataTable').find('tr > td:first-child input:checkbox')
	                .each(function(){
	                    var row = $(this).parents().eq(3);
	                    if ($(that).is(":checked")) {
	                        row.addClass('selected');
	                        $(this).prop('checked', true);
	                    } else {
	                        row.removeClass('selected');
	                        $(this).prop('checked', false);
	                    }
	                });
	            datatable.api().draw(false);
	        });

	        $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
	        function tooltip_placement(context, source) {
	            var $source = $(source);
	            var $parent = $source.closest('table');
	            var off1 = $parent.offset();
	            var w1 = $parent.width();

	            var off2 = $source.offset();
	            var w2 = $source.width();

	            if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
	            return 'left';
	        }
	        // dialogs
	        $( "#addreceivedialog" ).removeClass('hide').dialog({
	                resizable: false,
	                width: 'auto',
	                modal: true,
	                autoOpen: false,
	                position: { my: 'top', at: 'top+100' },
	                title: "Receive Items ",
	                dialogClass: 'fororderspage',
	                title_html: true,
	                buttons: [
	                    {
	                        html: "<i class='icon-save bigger-110'></i>&nbsp; Save",
	                        "class" : "btn btn-success btn-xs",
	                        click: function() {

								if($("#itemtabledetail tr").length > 1){
										saveItem(true);
								}
								else{
									alert("Please add items first");
								}
	                        }
	                    }
	                    ,
	                    {
	                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
	                        "class" : "btn btn-xs",
	                        click: function() {
								 if (confirm("Are you sure you want to cancel adding items?") == true) {
										$("#addreceivedialog")
										.find("input,select")
										.val('')
										.end();
										 var inc = $("#itemmodtable tr").length;
										 for(var i = 0; i <= inc; i++){
											removeItemInAddModal(i,"itemmodtable","newreceiveitemstotal");
										 }
										 $( this ).dialog( "close" );
										 $("#newreceiveitemstotal").val('');
										 $("#itemmodtable").empty();
									} else {

									}
	                        }
	                    }
	                ],
	                create: function( event, ui ) {
	                    // Set maxWidth
	                    $(this).css("maxWidth", "460px");
	                }
	        });
	        $("#additemtoreceivedialog").removeClass('hide').dialog({
			resizable: false,
			width: 'auto',
			modal: true,
			autoOpen: false,
			position: { my: 'top', at: 'top+100' },
			title: "Add Items in Receive",
			title_html: true,
			dialogClass: 'fororderspage',
			buttons: [
					{
						html: "<i class='icon-save bigger-110'></i>&nbsp; Save",
						"class" : "btn btn-success btn-xs itmc",
						click: function() {
							var my_data = $("#additemtoreceivedialog").data('param_1');
							if($("#additemmodtable tr").length >= 1){
									savenewitemtoreceive(my_data);
									reloadTable();
							}
							else{
								alert("Please add items first");
							}
						}
					}
					,
					{
						html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
						"class" : "btn btn-xs",
						click: function() {
							  if (confirm("Are you sure you want to cancel adding items?") == true) {
									$("#addItemsInCurrentOrder")
									.find("input,select")
									.val('')
									.end();
									 var inc = $("#additemmodtable tr").length;
									 for(var i = 0; i < inc; i++){
										removeItemInAddModal(i,"additemmodtable","additemstotal");
									 }
									 $( this ).dialog( "close" );
									 $("#additemstotal").val('');
									 $("#additemmodtable").empty();

							  }
						}
					}
				],
				create: function( event, ui ) {
					// Set maxWidth
					$(this).css("maxWidth", "460px");
				}

		});
	        $( "#editreceivedialog" ).removeClass('hide').addClass('editreceiveitems').dialog({
					resizable: false,
					width: 'auto',
					modal: true,
					autoOpen: false,
					title: "Edit Item",
					title_html: true,
					dialogClass: 'fororderspage',
					buttons: [
						{
							html: "<i class='icon-save bigger-110'></i>&nbsp; Update",
							"class" : "btn btn-success btn-xs",
							click: function() {
								saveItem(false);
							}
						}
						,
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancel",
							"class" : "btn btn-xs",
							click: function() {
								$( this ).dialog( "close" );
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "460px");
					}
	        });

	         $( "#stockreceivedialog" ).removeClass('hide').dialog({
					resizable: false,
					width: 'auto',
					maxWidth: '700px',
					modal: true,
					autoOpen: false,
					title: "Received Items",
					title_html: true,
					dialogClass: 'fororderspage',
					buttons: [
						{
							html: "<i class='icon-remove bigger-110'></i>&nbsp; Close",
							"class" : "btn btn-xs todofunc",
							click: function() {
								$( this ).dialog( "close" );
								  reloadGroupedData();
							}
						}
					],
					create: function( event, ui ) {
						// Set maxWidth
						$(this).css("maxWidth", "700px");
					}
			});
	         // populate category & supplier records in select boxes
						var supsel = $(".supselect");
						supsel.html('');
						supsel.append('<option class="supid-0" value="0">None</option>');
						for (key in suppliers){
							supsel.append('<option class="supid-'+suppliers[key].id+'" value="'+suppliers[key].id+'">'+suppliers[key].name+'</option>');
						}

					    var catsel = $(".catselect");
						catsel.html('');
						catsel.append('<option class="catid-0" value="0">None</option>');
						for (key in categories){
							catsel.append('<option class="catid-'+categories[key].id+'" value="'+categories[key].id+'">'+categories[key].name+'</option>');
						}


					// populate tax records in select boxes
					var taxsel = $(".taxselect");
					taxsel.html('');
					for (key in WPOS.getTaxTable().rules){
						taxsel.append('<option class="taxid-'+WPOS.getTaxTable().rules[key].id+'" value="'+WPOS.getTaxTable().rules[key].id+'">'+WPOS.getTaxTable().rules[key].name+'</option>');
					}

			$("#additemproduct").keyup(function(e){
				$("#suggesstion-box-receive-items ul").empty();
				var leng = $("#additemproduct").val().length;
				var table ="additemmodtable";
				var productfield="additemproduct";
				var finder = {};
				if(leng >=4){
					var find =  $("#additemproduct").val();
					if(find == "")
						return true;
					if(find == 0 && find.length ==1)
						return true;
					finder.find=find;
					finder.enter=0;

					result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					var newdata="";
					for(k in result)
					{
						$("#suggesstion-box-receive-items").show();
						 if (e.keyCode >= 48 && e.keyCode <= 57) {
							// Number
							var findvalue = result[k].code;
							//alert('number');

						} else if (e.keyCode >= 65 && e.keyCode <= 90) {
							// Alphabet upper case
							var findvalue = result[k].name;
							//alert("UPPER CASE");

						} else if (e.keyCode >= 97 && e.keyCode <= 122) {
							// Alphabet lower case
							var findvalue = result[k].name;
							//alert("LOWER CASE");
						}
						else if  (e.keyCode == 16) {
							// for shift key
							var findvalue = result[k].name;
						}
						else if (e.keyCode == 08) {
							//for backspace
							if (find.match(/[a-z]/i)) {
								// alphabet letters found
								var findvalue = result[k].name;
							}
							else{
								var findvalue = result[k].code;
							}
						}
						else{
							var findvalue = result[k].code;
						}
						var findvaluecode=result[k].code;
						if(e.keyCode!= 32){
							var ulli = '<li  onClick="return getDataSpecificItem(\''+findvaluecode+'\',\''+table+'\',\''+productfield+'\');">'+findvalue+'</li>';
						}
						$("#suggesstion-box-receive-items ul").append(ulli);
					}
				}
				// when enter is pressed
				if(e.keyCode === 13){

					//return false;
					var find =  $("#additemproduct").val();

					if(find == "")
					return true;
					var inc = $("#additemmodtable tr").length;

					for(var i=0; i < inc; i++){
						//additemmodtable
											//additemmodtablename0
						var existcode = $("#additemmodtablenewreceiveitemcode"+i+"").val();
						var existname = $("#additemmodtablenewreceiveitemname"+i+"").val();

						if(existname == find || existcode == find){
							alert("The item is already added");
							return true;
							//break;
						}
					}
						$onebyone = 1;
						finder.find=find;
						finder.enter=1;

					  //result = WPOS.sendJsonData("items/findsugesstions", JSON.stringify(finder));
					  result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));

					if(result != false){
						   addItemModifier(result,table,productfield);
					}
					else{
						if (find.match(/[a-z]/i)) {
							// alphabet letters found
							alert("Item not found, please enter barcode to add as new product.");
							$("#additemproduct").val("");
							$("#additemproduct").focus();
						}
						else{
							alert("Item with this barcode does not exist and now it will be added.");
							addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[]}},table,productfield);
							var inc = $("#"+table+" tr").length;

							$('#'+table+'newreceiveitemqty'+(inc-1)).focus();
						}
					}
				}

			    var inc = $("#additemmodtable"+" tr").length;
				$('div.ui-dialog-buttonpane.forcount').find('#itmtotalcount').remove();
		        $('div.ui-dialog-buttonpane.forcount').append('<div class="itmtotalcount" id="itmtotalcount">'+'Total :' + (inc) + ' Items </div>');

			});
			$("#newreceiveitemproduct").keyup(function(e){
				/*******
				*
				*
				* 12 June 2017 Dynamic suggestion list
				*
				*
				********/
				$("#suggesstion-box ul").empty();
				var leng = $("#newreceiveitemproduct").val().length;
				var table ="itemmodtable";
				var productfield="newreceiveitemproduct";
				var finder = {};
				if(leng >=4){
					var find =  $("#newreceiveitemproduct").val();

					$("#suggesstion-box ul").empty();
					if(find == "")
						return true;
					if(find == 0 && find.length ==1)
						return true;


					finder.find=find;
					finder.enter=0;
					result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));

					for(k in result)
					{
						$("#suggesstion-box").show();
						 if (e.keyCode >= 48 && e.keyCode <= 57) {
							// Number
							var findvalue = result[k].code;
							//alert('number');

						} else if (e.keyCode >= 65 && e.keyCode <= 90) {
							// Alphabet upper case
							var findvalue = result[k].name;
							//alert("UPPER CASE");

						} else if (e.keyCode >= 97 && e.keyCode <= 122) {
							// Alphabet lower case
							var findvalue = result[k].name;
							//alert("LOWER CASE");
						}
						else if  (e.keyCode == 16) {
							// for shift key
							var findvalue = result[k].name;
						}
						/*else if ( (e.keyCode >= 65 && e.keyCode <= 90 && !(e.keyCode == 16)) || (e.keyCode >= 97 && e.keyCode <= 122 && (e.keyCode == 16)) ) {
							 // alerts "CAPSLOCK is ON"
							 var findvalue = result[k].name;
						}*/
						else if (e.keyCode == 08) {
							//for backspace
							if (find.match(/[a-z]/i)) {
								// alphabet letters found
								var findvalue = result[k].name;
							}
							else{
								var findvalue = result[k].code;
							}
						}
						else{
							var findvalue = result[k].code;
						}
						var findvaluecode=result[k].code;
						if(e.keyCode!= 32){
							//var ulli = '<li  onClick="return getDataSpecificItem(\''+findvaluecode+'\');">'+findvalue+'</li>';
							var ulli = '<li  onClick="return getDataSpecificItem(\''+findvaluecode+'\',\''+table+'\',\''+productfield+'\');">'+findvalue+'</li>';
						}
						$("#suggesstion-box ul").append(ulli);
					}
				}
					// when enter is pressed
				if(e.keyCode === 13){

					//return false;
					var find =  $("#newreceiveitemproduct").val();

					if(find == "")
					return true;
					var inc = $("#itemmodtable tr").length;

					for(var i=0; i < inc; i++){

						var existcode = $("#itemmodtablenewreceiveitemcode"+i+"").val();
						var existname = $("#itemmodtablenewreceiveitemname"+i+"").val();
						if(existname == find || existcode == find){
							alert("The item is already added");
							return true;
							//break;
						}
					}

						finder.find=find;
						finder.enter=1;
					  result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
					  if(result != false){
						 addItemModifier(result,table,productfield);
					}
					else{
						if (find.match(/[a-z]/i)) {
							// alphabet letters found
							alert("Item not found, please enter barcode to add as new product.");
							$("#newreceiveitemproduct").val("");
							$("#newreceiveitemproduct").focus();
						}
						else{
							alert("Item with this barcode does not exist and now it will be added.");
							addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[]}},table,productfield);

						}


					}
				}
			});

			$("#receiveitemprice, #receiveitemcost, #newreceiveitemqty").keyup(function(){
			   calculateMarginTotalEDIT();

			});
			 $("#receiveitemqty").mouseup(function(){
			   calculateMarginTotalEDIT();
			});
			  var now = new Date();
			  var month = now.getMonth()+1;
			  if(month<10) {
					month='0'+month
			  }
			  var day = now.getDate();
			  if(day<10) {
					day='0'+day
			  }
			  var date = month+'/'+day+'/'+now.getFullYear();
			  $("#newreceiveitemdate").val(date);

			  $("#newreceiveitemdate").datepicker();

			// hide loader
	        WPOS.util.hideLoader();
	    });
		/*****
		*
		* Get data of a specific code or name
		*
		*****/
		function getDataSpecificItem(code,table,productfield){
			$("#suggesstion-box").hide();
			$("#suggesstion-box-receive-items").hide();
				var inc = $("#"+table+" tr").length;
				for(var i=0; i < inc; i++){
					//itemmodtablenewreceiveitemname0
					var existcode = $("#"+table+"newreceiveitemcode"+i+"").val();
					var existname = $("#"+table+"newreceiveitemname"+i+"").val();

					if(existcode == code.toUpperCase()){
						alert("The item is already added");
						return true;
					}
				}
				var finder={};
				finder.find=code;
				finder.enter=1;
				result = WPOS.sendJsonData("receive/find", JSON.stringify(finder));
				if(result != false){
					 addItemModifier(result,table,productfield);
					 var inc = $("#"+table+" tr").length;
					 $('#'+table+'newreceiveitemqty'+(inc-1)).focus();
					 var inc = $("#additemmodtable"+" tr").length;
					 $('div.ui-dialog-buttonpane.forcount').find('#itmtotalcount').remove();
					 $('div.ui-dialog-buttonpane.forcount').append('<div class="itmtotalcount" id="itmtotalcount">'+'Total :' + (inc) + ' Items </div>');
				}
				else{
					addItemModifier({"":{"code":find,"qty":"1","name":"","alt_name":"","description":"","taxid":"1","cost":"0.00","price":"0.00","supplierid":"0","categoryid":"0","type":"","modifiers":[]}});
				}
		}
	      function getReceiveItemsOfNumber(number,a,b,c,d,e,f,g){
			//a = count
			//b = invoice_total
			//c = totals_sum
			//d = margin
			//e = cost
			//f = price
			//g = Supplier name
			WPOS.util.showLoader();
			 $(function() {
			 stockhist = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: number}));

			//==================================================================================//
					var receiveitemarray1 = [];
					var tempitemreceive1;
					var taxrules = WPOS.getTaxTable().rules;
					for (var key in stockhist){
						tempitemreceive1 = stockhist[key];
						if (taxrules.hasOwnProperty(tempitemreceive1.taxid)){
							tempitemreceive1.taxname = taxrules[tempitemreceive1.taxid].name;
						} else {
							tempitemreceive1.taxname = "Not Defined";
						}
						receiveitemarray1.push(tempitemreceive1);
					}

					datatables = $('#detailInvoiceData').dataTable({
						"destroy": true,
						"bProcessing": true,
						"aaData": receiveitemarray1,
						"aaSorting": [[ 1, "desc" ]],
						"aLengthMenu": [ 10, 25, 50, 100, 200],
						"aoColumns": [
							{ "mData":null, sDefaultContent:'<div style="text-align: center"><label><input class="ace dt-select-cb" type="checkbox"><span class="lbl"></span></label><div>', bSortable: false },
							{ "mData":"id", sDefaultContent:'null' },
							{ "mData":"invoice_number", sDefaultContent:'null' },
							{ "mData":"name", sDefaultContent:'null' },
							{ "mData":"qty", sDefaultContent:'null' },
							{ "mData":function(data,type,val){return (data['cost']==""?"":WPOS.util.currencyFormat(data["cost"]));} },
							{ "mData":function(data,type,val){return (data['price']==""?"":WPOS.util.currencyFormat(data["price"]));} },
							{ "mData":"margin", sDefaultContent:'null' },
							{ "mData":"total" , sDefaultContent:'null'},
							{ "mData":function(data,type,val){return (categories.hasOwnProperty(data.categoryid)?categories[data.categoryid].name:'None'); } },
							{ "mData":function(data,type,val){return (suppliers.hasOwnProperty(data.supplierid)?suppliers[data.supplierid].name:'None'); } },
							{ mData:function(data,type,val){return '<div class="action-buttons"><a class="green" onclick="openEditDialog($(this).closest(\'tr\').find(\'td\').eq(1).text());"><i class="icon-pencil bigger-130"></i></a><a class="red" onclick=removeItem("'+data.id+'","'+data.invoice_number+'")><i class="icon-trash bigger-130"></i></a></div>'; }, "bSortable": false }
						],
						"columns": [
							{},
							{type:  "numeric"},
							{type:   "string"},
							{type:   "string"},
							{type:   "string"},
							{type:  "numeric"},
							{type: "currency"},
							{type:   "string"},
							{type:  "numeric"},
							{type:  "numeric"},
							{type:   "string"},
							{type:   "string"},
							{}
						],
						"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
							// Add selected row count to footer
							var selected = this.api().rows('.selected').count();
							return sPre+(selected>0 ? '<br/>'+selected+' row(s) selected <span class="action-buttons"><a class="red" onclick="removeSelectedItems();"><i class="icon-trash bigger-130"></i></a></span>':'');
						}
					});
					  // row selection checkboxes
							datatables.find("tbody").on('click', '.dt-select-cb', function(e){
								var row = $(this).parents().eq(3);
								if (row.hasClass('selected')) {
									row.removeClass('selected');
								} else {
									row.addClass('selected');
								}
								datatables.api().draw(false);
								e.stopPropagation();
							});

							$('table.dataTable th input:checkbox').on('change' , function(){
								var that = this;
								$(this).closest('table.dataTable').find('tr > td:first-child input:checkbox')
									.each(function(){
										var row = $(this).parents().eq(3);
										if ($(that).is(":checked")) {
											row.addClass('selected');
											$(this).prop('checked', true);
										} else {
											row.removeClass('selected');
											$(this).prop('checked', false);
										}
									});
								datatables.api().draw(false);
							});

							$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
							function tooltip_placement(context, source) {
								var $source = $(source);
								var $parent = $source.closest('table');
								var off1 = $parent.offset();
								var w1 = $parent.width();

								var off2 = $source.offset();
								var w2 = $source.width();

								if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
								return 'left';
							}

			//=================================================================================//
			});
			WPOS.util.hideLoader();
			$('div.ui-dialog-buttonpane').find('.todofunc').parent().parent().addClass('formainrow');

			$('#mainrow').remove();
		    $('#stockreceivedialog').append('<div id="mainrow" class="mainrowclass"><table  class="table"><thead><tr><th>Invoice Number</th><th>Total Items</th><th>Invoice Total</th><th>Total</th><th>Margin</th><th>Total Cost</th><th>Total Price</th><th>Supplier</th></tr></thead>'+'<tr><td>'+number+'</td><td>'+a+'</td><td>'+b+'</td><td>'+c+'</td><td>'+d+'</td><td>'+e+'</td><td>'+f+'</td><td>'+g+'</td></tr></table></div>');

			$("#stockreceivedialog").dialog('open');

		}
	    // updating records
	    function openEditDialog(id){
	        var item = stockhist[id];

	          $("#receiveitemid").val(item.id);
	          $("#receiveitemsupplier").val(item.supplierid);
	          $("#receiveitemcategory").val(item.categoryid);
	          $("#receiveitemnumber").val(item.invoice_number);
	          $("#receiveiteminvoicetotal").val(item.invoice_total);
	          $("#receiveitemcode").val(item.code);
	          $("#receiveitemname").val(item.name);
	          $("#receiveitemqty").val(item.qty);
	          $("#receiveitemcost").val(item.cost);
	          $("#receiveitemprice").val(item.price);
	          $("#receiveitemmargin").val(item.margin);
	          $("#receiveitemtotal").val(item.total);
	          $("#receiveitemdate").val(item.created);

	        $("#editreceivedialog").dialog("open");

	    }

	    var num = 1;
	    function addItemModifier(itemData,table,productfield){
			var inc = $("#"+table+" tr").length;
			var cat,tax;
			var totrows = inc+1;
			if(table == "itemmodtable"){
					 var totalfield = "newreceiveitemstotal";
				 }
				 else if(table == "additemmodtable"){
					 var totalfield = "additemstotal";
				 }
			if(JSON.stringify(itemData) === JSON.stringify({})){
	              $("#itemmodtable").append('<tr class="item"><td><input style="width : 90px" type="text" class="modname itemval" name="newreceiveitemcode"  id="newreceiveitemcode'+inc+'" value=""/><input type="hidden" id="itemstatus'+inc+'"  value="1"></td><td><input style="width: 200px" type="text" class="modname itemval" name="newreceiveitemname"  id="newreceiveitemname'+inc+'" value=""/></td><td><select id="newreceiveitemcategory'+inc+'" class="catselect"></select></td><td><input type="number" style="width: 60px" class="modminqty itemval" min="1"  name="newreceiveitemqty" id="newreceiveitemqty'+inc+'" value="1"/></td><td><select id="newreceiveitemtax'+inc+'" class="taxselect"></select></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="newreceiveitemcost" id="newreceiveitemcost'+inc+'" value="0.00"/></td><td><input type="text" style="width: 70px" name="newreceiveitemprice" id="newreceiveitemprice'+inc+'" class="modprice itemval" value="0.00"/></td><td><input type="text" style="width: 70px" name="newreceiveitemmargin" id="newreceiveitemmargin'+inc+'" class="modminqty itemval" value="0"/></td><td><input type="text" style="width: 50px" name="newreceiveitemsum" id="newreceiveitemsum'+inc+'" class="modminqty itemval" value="0" readonly/></td><td style="text-align: right;"><button id="removeadditem'+inc+'" class="btn btn-danger btn-xs" onclick="removeItemInAddModal('+inc+')">X</button></td></tr>');
			} else {
				 for (var i in itemData){
					  if(itemData[i].cost == null){
						 itemData[i].cost=0;
					 }
					 if(itemData[i].price == null){
						 itemData[i].price=0;
					 }

					 $("#"+table).append('<tr class="item"><td><input style="width : 90px" type="text" class="modname itemval" name="newreceiveitemcode"  id="'+table+'newreceiveitemcode'+inc+'" value="'+itemData[i].code+'" readonly="true"/><input id="'+table+'itemstatus'+inc+'" type="hidden" value="0"></td><td><input  type="text" class="modname itemval nameindialog" name="newreceiveitemname"  id="'+table+'newreceiveitemname'+inc+'" value="'+itemData[i].name+'"/></td><td><select id="'+table+'newreceiveitemcategory'+inc+'" class="catselect"></select></td><td><input type="number" style="width: 60px" class="modminqty itemval" min="1" onmouseup="reordermouseUp('+inc+',\''+table+'\',\''+totalfield+'\')" name="newreceiveitemqty" id="'+table+'newreceiveitemqty'+inc+'" value="'+itemData[i].qty+'"/></td><td><select id="'+table+'newreceiveitemtax'+inc+'" class="taxselect"></select></td><td><input type="text" style="width: 70px" class="modmaxqty itemval" name="newreceiveitemcost" onkeyup="reordermouseUp('+inc+',\''+table+'\',\''+totalfield+'\')" id="'+table+'newreceiveitemcost'+inc+'" value="'+itemData[i].cost+'"/></td><td><input type="text" onkeyup="reordermouseUp('+inc+',\''+table+'\',\''+totalfield+'\')" style="width: 70px" name="newreceiveitemprice" id="'+table+'newreceiveitemprice'+inc+'" class="modprice itemval" value="'+itemData[i].price+'"/></td><td><input type="text" style="width: 70px" name="newreceiveitemmargin" id="'+table+'newreceiveitemmargin'+inc+'" class="modminqty itemval" value="0"/></td><td><input type="text" name="newreceiveitemsum" id="'+table+'newreceiveitemsum'+inc+'" class="modminqty itemval" value="'+itemData[i].total+'" readonly/></td><td style="text-align: right;"><button id="'+table+'removeadditem'+inc+'" class="btn btn-danger btn-xs delbutton" onclick="removeItemInAddModal('+inc+',\''+table+'\',\''+totalfield+'\')">X</button></td></tr>');
			          tax = itemData[i].taxid; cat = itemData[i].categoryid;

						calculationMarginTotal(inc,table,totalfield);

						var catsel = $("#"+table+"newreceiveitemcategory"+inc);
						catsel.html('');
						catsel.append('<option class="catid-0" value="0">None</option>');
						for (key in categories){
								catsel.append('<option class="catid-'+categories[key].id+'" value="'+categories[key].id+'">'+categories[key].name+'</option>');
						}
						if(cat)
						$("#"+table+"newreceiveitemcategory"+inc).val(cat);


						// populate tax records in select boxes
						var taxsel = $("#"+table+"newreceiveitemtax"+inc);
						taxsel.html('');
						for (key in WPOS.getTaxTable().rules){
							taxsel.append('<option class="taxid-'+WPOS.getTaxTable().rules[key].id+'" value="'+WPOS.getTaxTable().rules[key].id+'">'+WPOS.getTaxTable().rules[key].name+'</option>');
						}
						if(tax)
						$("#"+table+"newreceiveitemtax"+inc).val(tax);
						/*
					   $("#"+table+"newreceiveitemprice"+inc+", #"+table+"newreceiveitemcost"+inc+", #"+table+"newreceiveitemqty"+inc).keyup(function(){
						   //calculationMarginTotal(inc);
						   calculationMarginTotal(inc,table,totalfield);
						});
						 $("#"+table+"newreceiveitemqty"+inc).mouseup(function(){
							 alert('gfdgf' +  inc );
						   //calculationMarginTotal(inc);
							calculationMarginTotal(inc,table,totalfield);
						}); */
						inc++;
			       }

			       $("#"+productfield).val("");

			 }
				   //$("#newreceiveitemcategory"+inc+"");

			num = num + 1;
	    }

	    function reordermouseUp(inc,table,totalfield){
			calculationMarginTotal(inc,table,totalfield)
		}
	    var modtableheader = '<thead class="table-header smaller"><tr><th><small>Default</small></th><th><small>Name</small></th><th><small>Price</small></th><th><button class="btn btn-primary btn-xs pull-right" onclick="addSelectModItem($(this).parents().eq(3).find(\'.modoptions\'));">Add Option</button></th></tr></thead>';
	    var modselectoption = '<tr><td><input onclick="handleSelectCheckbox($(this));" type="checkbox" class="modoptdefault"/></td><td><input style="width: 130px" type="text" class="modoptname" value=""/></td><td><input type="text" style="width: 60px" class="modoptprice" value="0.00"/></td><td style="text-align: right;"><button class="btn btn-danger btn-xs" onclick="$(this).parents().eq(1).remove();">X</button></td></tr>';

	    function addSelectModItem(elem){
	        $(elem).append(modselectoption);
				if (elem.find('tr').length==1) $(elem).find('.modoptdefault').prop('checked', true);
	    }

	    function handleSelectCheckbox(elem){
	        var table = $(elem).parent().parent().parent();
				table.find('.modoptdefault').prop('checked', false);
				$(elem).prop('checked', true);
	    }

	    function saveItem(isnewitem){
	        // show loader
	        WPOS.util.showLoader();
	        var item = {
				product : []
				 };
			var itemEdit = {};
	       var result,costval;

	        if (isnewitem){

				var rows = $("#itemmodtable tr").length;
				var i=0;
	            // adding a new item

	            item.supplierid = $("#newreceiveitemsupplier").val();
	            item.invoice_number = $("#newreceiveitemnumber").val();
	            item.invoice_total = $("#newreceiveitemtotal").val();
	            item.totals_sum = $("#newreceiveitemstotal").val();
	            item.created = $("#newreceiveitemdate").val();

	           for(i = 0; i < rows; i++){
				   var test = {}
				    test.status = $("#itemmodtableitemstatus"+i).val();
				    test.code = $("#itemmodtablenewreceiveitemcode"+i).val();
					test.name = $("#itemmodtablenewreceiveitemname"+i).val();
					test.categoryid = $("#itemmodtablenewreceiveitemcategory"+i).val();
					test.qty = $("#itemmodtablenewreceiveitemqty"+i).val();
					test.taxid = $("#itemmodtablenewreceiveitemtax"+i).val();
					costval = $("#itemmodtablenewreceiveitemcost"+i).val();
					test.cost = (costval ? costval : 0);
					test.price = $("#itemmodtablenewreceiveitemprice"+i).val();
					test.margin = $("#itemmodtablenewreceiveitemmargin"+i).val();
					test.total = $("#itemmodtablenewreceiveitemsum"+i).val();
					item['product'].push(test);
			   }
	          result = WPOS.sendJsonData("receive/add", JSON.stringify(item));

	            if (result!==false){
	                receivestock[result.id] = result;

	                reloadGroupedData();
	                // reloadTable();
	                $("#addreceivedialog").dialog("close");
	                $("#addreceivedialog")
					.find("input,select")
					.val('')
					.end();
					 var inc = $("#itemmodtable tr").length;
					  for(var i = 0; i <= inc; i++){
						//removeItemInAddModal(i);
						removeItemInAddModal(i,"itemmodtable","newreceiveitemstotal");
					 }
					  $("#newreceiveitemstotal").val('');
	            }
	        } else {

	            // updating an item
	            itemEdit.id = $("#receiveitemid").val();
	            itemEdit.supplierid = $("#receiveitemsupplier").val();
	            itemEdit.categoryid = $("#receiveitemcategory").val();
	            itemEdit.invoice_number = $("#receiveitemnumber").val();
	            itemEdit.invoice_total = $("#receiveiteminvoicetotal").val();
	            itemEdit.code = $("#receiveitemcode").val();
	            itemEdit.name = $("#receiveitemname").val();
	            itemEdit.qty = $("#receiveitemqty").val();
	            costval = $("#receiveitemcost").val();
	            itemEdit.cost = (costval ? costval : 0);
	            itemEdit.price = $("#receiveitemprice").val();
	            itemEdit.margin = $("#receiveitemmargin").val();
	            itemEdit.taxid = $("#receiveitemtax").val();
	            itemEdit.total = $("#receiveitemtotal").val();
	            itemEdit.totals_sum = $("#newreceiveitemstotal").val();
	            itemEdit.created = $("#receiveitemdate").val();

	            result = WPOS.sendJsonData("receive/edit", JSON.stringify(itemEdit));
	            if (result!==false){
	                receivestock[result.id] = result;
	                //reloadGroupedData();
	                reloadData(itemEdit.invoice_number);
	                $("#editreceivedialog").dialog("close");
	            }
	        }
	        // hide loader
	        WPOS.util.hideLoader();
	    }
		function savenewitemtoreceive(invoice_number){
			  WPOS.util.showLoader();
	        var item = {
				product : []
				 };
			var itemEdit = {};
	        var result,costval;

	        if (invoice_number){

				var rows = $("#additemmodtable tr").length;
				var i=0;
	            // adding a new item
	             var now = new Date();
				var month = now.getMonth()+1;
				if(month<10) {
						month='0'+month
				}
				var day = now.getDate();
				if(day<10) {
					day='0'+day
				}
				var date = month+'/'+day+'/'+now.getFullYear();
	            item.supplierid = $("#additemsupplier").val();
	            item.invoice_number = invoice_number;
	            item.invoice_total =  $("#additeminvoicetotal").val();
	            item.totals_sum = $("#additemstotal").val();
	            item.created = date;


	           for(i = 0; i < rows; i++){
				   var test = {}
				    test.status = $("#additemmodtableitemstatus"+i).val();
				    test.code = $("#additemmodtablenewreceiveitemcode"+i).val();
					test.name = $("#additemmodtablenewreceiveitemname"+i).val();
					test.categoryid = $("#additemmodtablenewreceiveitemcategory"+i).val();
					test.qty = $("#additemmodtablenewreceiveitemqty"+i).val();
					test.taxid = $("#additemmodtablenewreceiveitemtax"+i).val();
					costval = $("#additemmodtablenewreceiveitemcost"+i).val();
					test.cost = (costval ? costval : 0);
					test.price = $("#additemmodtablenewreceiveitemprice"+i).val();
					test.margin = $("#additemmodtablenewreceiveitemmargin"+i).val();
					test.total = $("#additemmodtablenewreceiveitemsum"+i).val();
					item['product'].push(test);
			   }
	          result = WPOS.sendJsonData("receive/additemtoinvoice", JSON.stringify(item));

	            if (result!==false){
	                //receivestock[result.id] = result;

	                reloadGroupedData();
	                // reloadTable();
	                $("#additemtoreceivedialog").dialog("close");
	                $("#additemtoreceivedialog")
					.find("input,select")
					.val('')
					.end();
					 var inc = $("#additemmodtable tr").length;
					  for(var i = 0; i <= inc; i++){
						//removeItemInAddModal(i);
						removeItemInAddModal(i,"additemmodtable","additemstotal");
					 }
					  $("#additemstotal").val('');
	            }
	        }
			 // hide loader
	        WPOS.util.hideLoader();
		}
	    function removeItemInAddModal(val,table,totalfield){
			var sum = $("#"+table+"newreceiveitemsum"+val+"").val();
			$("#"+table+"removeadditem"+val+"").parent().parent().remove();
			var total = $("#"+totalfield).val();
			var newtotal = total-sum;
			 newtotal = newtotal.toFixed(2);
			 $("#"+totalfield).val(newtotal);
			 var inc = $("#"+table+" tr").length;
			 if(inc>0){
				 $("#"+table+" tr").each(function(i, obj) {
						$(this).find('td').each (function() {
						  // do your cool stuff
							var td = $(this).find("input").attr('id');
							var tdselect = $(this).find("select").attr('id');
							var tdbutton = $(this).find("button").attr('id');

							if(td){
								var res = td.replace(/\d+/g, '');
								var findtype = 'input';
								switch(res){
									case table+'newreceiveitemqty' :
										$(this).find('input').attr('onmouseup','reordermouseUp(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
										break;
									case table+'newreceiveitemcost':
										$(this).find('input').attr('onkeyup','reordermouseUp(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
										break;
									case table+'newreceiveitemprice':
										$(this).find('input').attr('onkeyup','reordermouseUp(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
										break;
								}
							}
							else if(tdselect){
								var res = tdselect.replace(/\d+/g, '');
								var findtype = 'select';
							}
							else if(tdbutton){
								var res = tdbutton.replace(/\d+/g, '');
								var findtype = 'button';
								$(this).find('button').attr('onclick','removeItemInAddModal(\''+i+'\',\''+table+'\',\''+totalfield+'\')');
							}
							$(this).find(findtype).attr('id',res+i);
						});
				});
			}
			var inc = $("#additemmodtable"+" tr").length;
			$('div.ui-dialog-buttonpane.forcount').find('#itmtotalcount').remove();
		    $('div.ui-dialog-buttonpane.forcount').append('<div class="itmtotalcount" id="itmtotalcount">'+'Total :' + (inc) + ' Items </div>');
		}

		function calculationMarginTotal(inc,table,totalfield){

			  var cost = $("#"+table+"newreceiveitemcost"+inc).val();
			  var price = $("#"+table+"newreceiveitemprice"+inc).val();
			  var qty = $("#"+table+"newreceiveitemqty"+inc).val();

			   CostQty = (Number(cost)*Number(qty)).toFixed(2)
			  $("#"+table+"newreceiveitemsum"+inc).val(CostQty);

				   if(cost != "" && price!= "" ){
					   margin = ((Number(price)-Number(cost))/Number(price)) * 100;
					   margin = margin.toFixed(2)
							if(margin == "-Infinity" || isNaN(margin)){
								$('#'+table+'newreceiveitemmargin'+inc).val(0);
							}
							else{
								$('#'+table+'newreceiveitemmargin'+inc).val(margin+"%");
							}
				   }else{
					   $("#"+table+"newreceiveitemmargin"+inc).val(0);
				   }

			 var invoice_total = 0;
			 var rows = $("#"+table+" tr").length;
			      for(var i = 0; i < rows; i++){
					var val=$("#"+table+"newreceiveitemsum"+i).val();
					invoice_total = Number(invoice_total) + Number(val);
				}
				 invoice_total = invoice_total.toFixed(2)
				$("#"+totalfield).val(invoice_total);
		}

		function calculateMarginTotalEDIT(){
			  var cost = $("#receiveitemcost").val();
			  var price = $("#receiveitemprice").val();
			  var qty = $("#receiveitemqty").val();
			   totalEdit = (Number(cost)*Number(qty)).toFixed(2);
			  $("#receiveitemtotal").val(totalEdit);

				   if(cost != "" && price!= "" ){
					   margin = ((Number(price)-Number(cost))/Number(price)) * 100;
					   margin = margin.toFixed(2);
							if(margin == "-Infinity" || isNaN(margin)){
								$('#receiveitemmargin').val(0);
							}
							else{
								$('#receiveitemmargin').val(margin+"%");
							}
				   }else{
					   $("#receiveitemmargin").val(0);
				   }
		}

	    function removeItem(id, number){
		    var answer = confirm("Are you sure you want to delete this item?");
	        if (answer){
	            // show loader
	            WPOS.util.hideLoader();
	            if (WPOS.sendJsonData("receive/delete", '{"id":'+id+'}')){
	                delete receivestock[id];
	                reloadData(number);
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
	    }

	    function removeSelectedItems(){
	        var ids = datatables.api().rows('.selected').data().map(function(row){ return row.id });

	        var answer = confirm("Are you sure you want to delete "+ids.length+" selected items?");
	        if (answer){
	            // show loader
	            WPOS.util.hideLoader();
	            if (WPOS.sendJsonData("receive/delete", '{"id":"'+ids.join(",")+'"}')){
	                for (var i=0; i<ids.length; i++){
	                    delete receivestock[ids[i]];
	                }
	                reloadData(number);
	            }
	            // hide loader
	            WPOS.util.hideLoader();
	        }
	    }

	    function reloadData(number){
	        receivestock = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: number}));
	        reloadTable();
	    }

	    function reloadTable(){
	        var receiveitemarray1 = [];
	        var tempitemreceive1;
	         for (var key in receivestock){

	            tempitemreceive1 = receivestock[key];
	            receiveitemarray1.push(tempitemreceive1);
	        }
	        datatables.fnClearTable(false);
	        if(receiveitemarray1.length > 0){
	            datatables.fnAddData(receiveitemarray1, false);
			}
	        datatables.api().draw(false);
	    }

	     function reloadGroupedData(){
			   receivestock = WPOS.sendJsonData("receive/getGroupedData", JSON.stringify({"receive/getGroupedData":""}));
	        //receivestock = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: number}));
	        reloadGroupedDataTable();
	    }

	     function reloadGroupedDataTable(){
	        var receiveitemarray = [];
	        var tempitemreceive;
	         for (var key in receivestock){

	            tempitemreceive = receivestock[key];
	            receiveitemarray.push(tempitemreceive);
	        }
	        datatable.fnClearTable(false);
	        datatable.fnAddData(receiveitemarray, false);
	        datatable.api().draw(false);
	    }

		    var eventuiinit = false;

	    function initModalLoader(title){
	        $("#modalloader").removeClass('hide').dialog({
	            resizable: true,
	            width: 400,
	            modal: true,
	            autoOpen: false,
	            title: title,
	            title_html: true,
	            closeOnEscape: false,
	            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
	        });
	    }

	    function showModalLoader(title){
	        if (!eventuiinit){
	            initModalLoader(title);
	            eventuiinit = true;
	        }
	        $("#modalloader_status").text('Initializing...');
	        $("#modalloader_substatus").text('');
	        $("#modalloader_cbtn").hide();
	        $("#modalloader_img").show();
	        $("#modalloader_prog").show();
	        var modalloader = $("#modalloader");
	        modalloader.dialog('open');
	    }

	    function setModalLoaderProgress(progress){
	        $("#modalloader_progbar").attr('width', progress+"%")
	    }

	    function showModalCloseButton(result, substatus){
	        $("#modalloader_status").text(result);
	        setModalLoaderSubStatus(substatus? substatus : '');
	        $("#modalloader_img").hide();
	        $("#modalloader_prog").hide();
	        $("#modalloader_cbtn").show();
	    }

	    function setModalLoaderStatus(status){
	        $("#modalloader_status").text(status);
	    }

	    function setModalLoaderSubStatus(status){
	        $("#modalloader_substatus").text(status);
	    }

	    function editCurrentReceive(id){

			 $("#additemmodtable").empty();
	         $("#additemstotal").val('');
	         receiveData = WPOS.sendJsonData("receive/get", JSON.stringify({invoice_number: id}));
	         for(i in receiveData){
					 invoice_number = receiveData[i].invoice_number;
					 invoice_total = receiveData[i].invoice_total;
					 created = receiveData[i].created;
					 supplierid = receiveData[i].supplierid;
			 }
			     $("#additemmnumber").val(invoice_number);
			     $("#additeminvoicetotal").val(invoice_total);
			     $("#additemdate").val(created);
			     $("#additemsupplier").val(supplierid);
	         addItemModifier(receiveData,"additemmodtable","additemproduct");
			 $("#additemtoreceivedialog").data("param_1",id).dialog("open");
			 //next().children()
			  $('div.ui-dialog-buttonpane').find('.itmc').parent().parent().addClass('forcount');
			 var inc = $("#additemmodtable"+" tr").length;
			 $('div.ui-dialog-buttonpane.forcount').find('#itmtotalcount').remove();
		     $('div.ui-dialog-buttonpane.forcount').append('<div class="itmtotalcount" id="itmtotalcount">'+'Total :' + (inc) + ' Items </div>');

		}
	</script>
	<div id="modalloader" class="hide" style="width: 360px; height: 320px; text-align: center;">
	    <img id="modalloader_img" style="width: 128px; height: auto;" src="/admin/assets/images/cloud_loader.gif"/>
	    <div id="modalloader_prog" class="progress progress-striped active">
	        <div class="progress-bar" id="modalloader_progbar" style="width: 100%;"></div>
	    </div>
	    <h4 id="modalloader_status">Initializing...</h4>
	    <h5 id="modalloader_substatus"></h5>
	    <button id="modalloader_cbtn" class="btn btn-primary" style="display: none; margin-top:40px;" onclick="$('#modalloader').dialog('close');">Close</button>
	</div>
	<style type="text/css">
	    #receiveitemstable_processing {
	        display: none;
	    }

	    body.dragging, body.dragging * {
	        cursor: move !important;
	    }

	    .dragged {
	        position: absolute;
	        opacity: 0.8;
	        z-index: 2000;
	    }

	    #dest_table li.excluded, #source_table li.excluded {
	        opacity: 0.8;
	        background-color: #f5f5f5;
	    }

	    .placeholder {
	        position: relative;
	        height: 40px;
	    }

	    .placeholder:before {
	        position: absolute;
	        /** Define arrowhead **/
	    }
	</style>
